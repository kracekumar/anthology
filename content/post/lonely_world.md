+++
date = "2018-11-24T23:15:21+05:30"
title = "Lonely world"
tags = ["free-verse", "musing"]
+++

It’s a lonely world <br/>
Even your sleep is sold<br/>
There is no one to hold<br/>
Everything you touch is cold.<br/>
<br/>
It’s dark inside and bright outside <br/>
Thousands of friends in the network<br/>
But no one to share the table<br/>
Like a single flower vase.<br/>
<br/>
Like withered leaves,<br/>
the life changes the color,<br/>
to decrease the living will,<br/>
And fly high to cover the dead.<br/>
<br/>
Absorb all the knowledge <br/>
Like light falling on a plant,<br/>
Keep swallowing till you can,<br/>
And finally, call it off<br/>
when you’re full.<br/>
<br/>
It’s a lonely world,<br/>
Not a holy society,<br/>
Full of folly beings,<br/>
Carrying gally feelings everywhere.<br/>
<br/>
It’s full of crowd,<br/>
In a lonely world.<br/>
Alone in the crowd,<br/>
Walking drowned.<br/>
<br/>
Thoughts are fillers of the mind<br/>
Guiding day to day to nowhere<br/>
In a buried, hidden underpass,<br/>
It’s a lonely world.<br/>

