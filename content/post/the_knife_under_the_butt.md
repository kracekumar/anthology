+++
date = "2018-04-29T11:42:21+05:30"
title = "The knife under the butt"
tags = ["short story"]
+++

It was five past nine, Nila was ready to leave the house for high school. She pulled her sling bag lying under the bed, moved from living room to the house entrance, and said “Amma, I’m leaving for school and will be back for lunch,” in Tamil. From the kitchen, her mother replied, “Fine, see you in the afternoon.”

Guhan was out in the backyard playing with a rubber ball. He was alone in the backyard hitting the rubber ball against the compound wall. The coconut mill compound wall stretched long. The house was alone behind the mill. 

Every day he played in the backyard before leaving for school. His mother shouted from outside bathroom, “Guhan, water is warm, you can bath.” His mother murmured, how long can he play alone in the backyard?. “I’ll come in five minutes” replied Guhan in unenthusiastic voice.

A long bell sound ushered from the coconut mill indicating the time for the morning shift. The crowd from parts of the countryside walked in the mill. Most of the mill workers were women in thirties and forties.

Guhan grabbed a note lying on the table. He didn’t notice the note belonged to her sister. He wore the slippers and called out his mom for lunch box. His mom stepped out of the kitchen and handed over the silver lunch box. Amma, “I’ll come back home only at 7. I have a class match in the evening.” His mom replied with care, “Sure, return home before your elder brother returns.” He replied, “Seri, Seri” in Tamil.

Sundari went into the kitchen, lit up the gas, picked a water boiler and emptied the cow milk into the boiler. The milk boiled for fifteen minutes and she switched off the gas. In next ten minutes, she prepared the tea and poured into the ever silver tea container. Then she picked up three packs of plastic teacups from the shelf and placed down the floor. She walked to the bathroom and took a bath in the cold water. 

While she was still in the bathroom, her husband in his forties arrived in TVS 50 and stopped in front of the house under a coconut tree. He called out, “Sundari, is the tea ready?.”  A voice from the bathroom reached Maruthan’s ear, “it’s been ready a long back, and in the kitchen. You’re late today; I’ll be back there in two minutes.” Maruthan sat on the chair on the veranda. Sundari entered the kitchen from the back entrance of the house. She tightened the tea container mouth and picked the plastic cups on the left hand. Maruthan heard the footsteps of his wife, stood up from the plastic chair and collected the tea container and plastic cups. 

He gazed at her face with love and asked, “ Did you take a head bath?” She smiled and replied, “was it hard to find out from the looks?”. She walked backed to the kitchen to pick up the lentil vadai wrapped in  Tamil newspaper.

Maruthan picked up the tea container and placed on the iron base at the back of the motor vehicle. Then he tightened nylon rope across the silver container. He opened the cloth bag on the hook of the handlebar and inserted three packs of the plastic cups. Few plastic cups protruded from the cloth bag.

He got on to the vehicle and straightened the handlebar and turned the key on. Sundari walked out of the house and wrapped vadai. While he placed wrapped vadai in the cloth bag, she teasingly asked, “will you be able to sell all the tea before lunchtime?”.  With the grinning face, he replied, “who don’t want to have your tea?”. 

Both exchanged love smiles, and he informed, "it’s getting late, I’ll leave now to reach in ten minutes."

Thiru V.K Nagar was the prime location in and around villages because of the government higher secondary school. Thiru. V. K expanded to Thiru. V. Kalyanasundaram. Close too two thousand students studied in the school. Next state-run high second school was thirty kilometers away from this. The school had classes from the sixth standard to twelveth standard. The school was spread out in two acres. Most of the area was the playground for boys and girls with separate cricket ground, volley ground, football ground, and empty spaces. 

Maruthan reached the school and interval break bell rang. He picked up cloth bag in the left hand and tea container in right hand, walked towards to the staff in the ground floor.

He uttered, vanakkam to all the staff members, and distributed a tea and a vadai. Some staffs handed over the cash and others settled the bill monthly once. After ten minutes, he moved out of the staff room and entered the headmaster room. The room was spacious with tens of people to stand and few benches across the room. He handed over the tea and vadai to her with utmost reverence. She smiled back. Then he rushed to the first floor to distribute the tea to higher secondary staffs. First-floor staff room was small with fifteen teachers.

He handed over the tea to everyone, and one of the teachers informed him, his elder son Dhileeban missed the morning class. It wasn't new to him. It was usual of him. He never likes to voice against the sons. While walking back from the first floor to ground floor in steps, he recollected the movie playing in the local theatre. The movie was Guna and convinced himself; his elder son must have skipped the class for the film.

Under the tamarind tree, Maruthan stood next to his two-wheeler. The tamarind trees, neem trees were on the left right and right side of the school entrance. All the part-time street vendors and visitors parked the vehicles beneath the trees. Students walked in and out of the school in groups. Few students purchased tea. Maruthan noticed Guhan with his friend Anand and called him by his name. Guhan walked towards his dad, asked his dad, “what dad?”. He handed over two vadai to him. Guhan grabbed both the vadai and walked towards Anand. Both of them shared a vadai and walked towards the classroom.

At noon, the lunch bell broke, and students spearheaded to the playground. In next half an hour after finishing the meal, male students started playing cricket in different parts of the ground. Girls played volleyball and handball.

Nila reached the house for lunch. She had an hour to eat the lunch and get back to school. She was in the seventh standard. 

Every class had a collective cricket bat. All the students pooled the money to the joint bat. Sometimes the cricket bat was stolen from the shop. These students walked into the store in large groups, purchased rubber balls, stumps and a cricket bat asking hundreds of question to store owner and picked up a bat without owner’s notice. It took several months for the owner to figure out a bat was missing.

Every year, eleventh standard students organized unofficial cricket tournament. The participating classes were eighth, ninth and tenth standard for a total sixteen teams. Guhan was in the ninth standard C section. That day match was between his class and eight standard D section. The winner plays in the semi-final. The game was twelve overs for each side. First half is on lunch break and the second half is on evening post-school time.

Guhan along with Pachai opened the batting. Everyone on the team called Guhan the wild player. Guhan only believed in big hits. He hardly scored singles or twos, unless he decides to retain the strike for next over, which was true in most of the times. Every disliked batting with him for the same reason. Guhan scored quick twenty runs in ten balls. He was content with his performance and kept talking about it throughout the day. At the end of the first session, Guhan’s team scored ninety runs in twelve overs.

Anand and Guhan roamed together in the school. And we’re best buddies. He repeated his dismissal scene to him, a couple of times. Irritated by his repetition, Anand replied, “more than numbers of runs you score you spoke about the dismissal.”

The second session continued at four o clock and lasted till five ten. The eight standard lost by ten runs. Guhan had dropped a catch. Anand, Pachai, and Guhan ganged around the ground till six-thirty evening. Both of them teased Guhan for dropping the catch.

The last bus to Anand’s village was at seven. Pachai was from the next village. In a day, five times, the bus visits their village. Anand and Pachai traveled together every day. After sending off Anand, Guhan returned home at seven fifteen in the night.

The entire family except Guhan were watching the Tamil TV serial. Guhan cleaned his foot in the bathroom and entered the house from the back entrance. Dhileeban heard the noise of opening a snack box and said, “a creature entered the house, and entire snacks will disappear in minutes.”

Nila and Sundari laughed to the fullest and understood tonight there is a show from Dhileeban. The advertisement was on the television when Guhan entered the hall. Dhileeban looked at his younger brother’s face, and called Nila, “Your younger brother cleaned the snack box, and leftover is on his cheek” Guhan didn’t reply anything to his elder brother because of fear.

Guhan looked around to sit in a chair and found an empty in the opposite corner of the room entrance. He walked towards the chair, at the same time his elder brother crawled towards the chair behind the bed and put a hand on the chairs. Dhileeban pulled the chair without noise, Guhan failed to notice the move and slipped down the floor and hurt his bum. Out of feat, Guhan remained silent and shook his head sideways.

Dhileeban jumped up and down and mocked at Guhan. Nila had a belly laugh, and his mother said, “Dei, the elder son, can you stop mocking him? Enough for the day”. Guhan left the room feeling humiliated to the bathroom. 

The father requested his elder son to remain calm. The everyday scene was similar, elder brother mocks his younger brother to the fullest and shows unfettered care to his sister. The elder brother always speaks cuss word to his younger brother and Guhan is in fear of his elder brother all the time.

In the night, Sundari and Maruthan slept in the hall, and their children slept in the bedroom. Nila slept in the bed, and her brothers slept on the floor.

Next day, Dhileeban and Nila woke up early. Their mother had prepared the tea, both of them had a drink and sat in the hall for using the restroom. When Nila returned from the latrine, Dhileeban giggled and said, “Last night Guhan snored like a pig. Did you hear it?”

Their mother hearing the remark, “Dei, Dhileeban don’t start your foul remark so early in the day, can you let him sleep?”. 

He bounced back, “I don’t know how many ears you possess.”

Nila remarked, “ There were few power cuts last night. I couldn’t sleep well”. 

Dhileeban replied, “Yes, during every power cut, the snoring intensified, echoed in the room and I kicked him a couple of times in his bum, and he didn’t bother to respond.”

“He must be tired after playing” replied Nila.

“Oh, yes. He’ll be tired of eating food; today, he will swallow ten Idlis in the time you finish four Idlis. Watch him eat in the morning, we’ll continue the conversation in the evening”, said Dhileeban.

Nila didn’t offer a reply. After leaving the chair, Dhileeban entered the living room and changed his night clothes. In the night, he wore same pant and a different shirt. He pulled the white shirt in the hanger and altered. The shirt collar carried a sweat line. It’s his third wear for the week. He went out with the same brown full pant which he wore yesterday.

As he stood outside the house after quick breakfast looking for slippers, Nila came out and questioned, “when is your next turn to take bathe?”. Dhileeban giggled and left the place.

Nila went back inside the living room, and opened her bag, glanced at the timetable and placed the relevant books and notes inside. Meanwhile, Guhan entered the room after bathing with a wet towel wrapped in the shoulder.

“You bathed early. What’s the matter?”, Nila asked in a sarcastic tone. “Anand will reach school early after helping his father transport tomatoes to the market,” replied Guhan in a serious tone. “The elder brother informed me, last night was full of your loud snores,” said Nila.

“I don’t remember snoring. His day is incomplete without complaining about me”, replied Guhan.

Nila remained silent, and Guhan picked a notebook from the table and moved into the kitchen, and asked his mother, “Amma (mom) is breakfast ready?.”

Nila from the living room heard the question and said, “Anna (Brother), wait for 5 minutes, I’ll be back from the bath. It’s been a long time; we had breakfast together.”

Guhan without thinking said, “Sure, come back soon.”

His mother looked at him in the kitchen and remarked, “your sister is showering extra care today. Be careful. She and elder brother were discussing you when you were in the bathroom.”

“Let it be, Amma, leave it” Guhan replied in hunger. And further asked, “what’s have you prepared for the breakfast?”.

“Idli with peanut chutney, coriander chutney, and Lentils Sambhar,” replied Sundari with a smile. Coriander chutney was his favorite. He moved closer to his mother, opened each vessel and tasted a bit of all the items. By now, Nila returned to the living room and changed to school uniform.

Guhan sat on the floor. His mother placed two plates, one in front of him and one adjacent to him. She moved the hot box with Idlis and vessels with chutneys and Sambhar. 

Nila sat adjacent to her brother. Sundari first served Idlis to Guhan, Nila said, “Like every other time, you filled your son’s plate first.” Her mom replied, “You came late.” 

Guhan’s plate had five Idlis, Sambhar, peanut chutney, and extra coriander chutney. Nila took a small piece of idli peanut chutney and sambhar. Guhan ate hot Idlis quick whereas Nila took her own time to finish four Idlis which her mom placed during the first round.

Guhan rose up from the floor, picked up the plate, placed it in the sink, took a mug of water from the circular plastic water tub, washed his hand, and dropped the mug into the water tub.

After Guhan left the room, Nila rose up and washed her hand. She moved closer to her mother and whispered, “elder brother was correct about younger brother.”  Sundari pounced back, “Madam should get ready for school and leave now”.

The way to school was filled with students walking on both sides of the road in wearing the uniform and carrying the bags. The students walked in groups gossiping on all sorts of topics. Guhan walked alone towards the school without joining any group. Smiles filled everyone's face, the heat and untidiness never bothered them. He stopped beneath a gigantic tamarind tree which was in front of the market. 

The market was close to school a few hundred meters away. All the town bus stopped in front of the market and conductors helped passengers carrying the tomatoes and vegetable in the crate.

The tamarind tree was the rendezvous spot for Anand and Guhan. Anand joined Guhan with sweating face and oiled hair. He wore a white shirt with a free top button. The shirt had noticeable brown spots. The brown pant lost its shine. 

Together they walked to the school. After washing the face, Anand and Guhan walked into the classroom. Both of them crossed classmates reading and humming Tamil songs from the books.

The class had three columns of wooden benches. Pachai, Anand, and Guhan sat on the fourth bench in the second column. Each column had eight benches aligned one after the other with space to walk between them.

Guhan always sat between Pachai and Anand. The first bell rang to indicate the prayer is due in five minutes. Pachai pulled out a pocket folding knife from the bag. The knife was an old one with brown holder.

Guhan pulled the knife from Pachai. He opened and closed the blade. He poked the knife tip in his palm and felt no pain. “Pachai, It’s blunt knife, why did you bring this?”, asked Guhan. “No specific reason, my mom had cleaned the attic last evening, and I found this one lying in the utility box,” replied Pachai. The prayer bell rang for thirty seconds, and everyone stood up in the class. Guhan put the knife in the open bench drawer.

The prayer lasts for ten minutes. The class teacher walked in after the prayer and marked status in the student’s class register. During the first hour of the day, Anand pulled the knife from the bench drawer and played around with it. The class teacher noticed him not listening to the class and didn’t bother to draw his attention back to the class.

Two periods lapsed, and interval break was in place. All three them went to the boy's toilet and was back in the class before the bell. Pachai and Guhan shared the extra lentil Vadai which Guhan’s father handed over to them. 

Anand walked around the class talking with other friends. Guhan pulled the knife in and out of the case. “Do you feel like a hero?”, Pachai asked Guhan. Guhan replied, “No. I like the feel of folding and unfolding the knife.”

The interval bell rang, few students walked towards their seats. Anand moved into the bench from the left side. Guhan, moved close to Anand, and asked, “are we playing bet match, today evening?”. Anand faced Guhan, replied, “most probably, yes” and sat on the open knife in Guhan’s hand.

The blunt knife ran into the rectum, and unable to bear the intolerable pain, Anand shouted, “Amma, you son of an ass” and fell on top of the bench. Everyone rushed towards Anand and Guhan started sweating and touched Anand. Anand with eyes closed and shouted, “Don’t touch me the bastard” in pain and frustration. 

In twenty minutes, few students rushed to the classroom with a stretcher. The ambulance was waiting in the main gate. Anand cried, shouted out of pain while his friends lifted him and placed on the stretcher. Back of his pant was wet with blood.

Having heard what happened, Guhan's father went along with Anand in the ambulance. Once ambulance left everyone surrounded Guhan and shouted at him. Everyone abused his mother with all cuss words. Guhan left the school in the afternoon and didn’t return home for lunch.

The sun had set, darkness had taken over the night, he walked towards the home. It was pitch dark because of power cut in the area. The darkness filled Guhan’s mind and body. His heart was still palpitating, and he heard the news, Anand is safe and need to be in bed for a month.

He didn’t know the significance of the day.  He entered the house from the backside and stayed in the toilet till late night. He joined the house from back entrance at dinner time while everyone was in the kitchen.

After finishing the dinner, the elder brother hit Guhan in the head with his hand and asked him from the kitchen to move into the living room. Everyone one in the house knew what is expected to occur, but no had any guts to speak up.

The next day, Guhan woke up with bruises and all over the body. His hands were full of dark red impressions. After breakfast, his father handed him over three hundred rupees and instructed him to leave to grandparents place. He left the house with few clothes without realizing end of his school days.
