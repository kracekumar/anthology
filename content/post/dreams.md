+++
date = "2019-06-20T23:25:21+05:30"
title = "Dreams"
tags = ["free-verse", "poem"]
+++

No one bought his dreams<br/>
After death<br/>
He sowed them in the graveyard<br/>
