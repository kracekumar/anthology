+++
date = "2017-03-18T15:35:21+05:30"
title = "The uneven road"
tags = ["rumble"]
+++
It was 8 in the night and public was walking on the road, four wheelers were marching, and security guards of the apartments were rounding the streets. It’s a part of multiple ‘L’ shaped road connecting Outer Ring Road and Sarjapur Road. The significant amount of vehicles drives along this path to beat traffic. No more the road can be called an inner or secondary road. Every resident and by passer knows this shortcut.

This road is a slope. Tall verticals, a barren land filled with weed guarded by hollow block stones and few remaining coconut trees occupied the both sides of the road in one stretch. On the other end of the stretch of many apartments are three floors, four floors, five floors and traditional single floored houses.

The yellow sodium street light was glowing and shining like a bar of gold in front of every brick house and apartment. The stretch had 10 street lights from one side and eight from another side. The junction of the L-shaped road had a 20 highly packed plastic tarpaulins in few cents. These houses had been here for few years, and people live here in a bunch with no electricity.

The street light in front of the settlement fused out a few weeks back. No one from Electricity Board visited to replace the fused bulb. The people from settlement can’t pay thick currency notes and don't make enough to move out of the illegally occupied place. The other apartment dwellers or security guard discarded the dysfunctioning bulb.
