+++
date = "2019-07-06T12:50:21+05:30"
title = "Rivers Remember Book Review"
tags = ["book review", "rivers"]
+++

All the greatest civilization rose on the river banks:  Mesopotamia on Tigrus and Euphrates, Egypt on the Nile, Indus Valley on Indus River,  Huang-He River Civilization on Yellow River. 

<img src="https://upload.wikimedia.org/wikipedia/commons/1/13/Chennai-floods-2015-dec-1.jpg">

Singara Chennai is the capital of Tamil Nadu state located on the Coromandel coast of Bay of Bengal with a population close to eight lakhs.  First week of December 2015, due to a torrential downpour of North-East monsoon Singara Chennai submerged became Sinkana(submerged) Chennai. The book `Rivers Remember` covers the reasons of the floods, aftermath effects of flooding of three rivers in Chennai: Adayar, Coovum and Kosathalaiyar, human suffering, and human dignity of the human existence during havoc.


Tamil Nadu is one of the highly urbanized states in India. As per the Socio-Economic and Caste Census 2011, 42.47% of households are in urban areas [1]. The author of the book builds a narrative to disprove the claim made it's `one in every hundred years` rainfall that caused the floods.  The real cause for the floods is a bureaucratic delay in seeking permission to release steady water, coordination failure among PWD(Public Works Department), TNEB(Tamil Nadu Electricity Board), and Police Department, failure to indicate a warning to city residents, and encroachment of water bodies throughout the city. There is a case presented how The Urban Development Authority always bends the rules for new high rise building. Of all these, the TN government hadn't developed a disaster management plan! 

<img src="https://pbs.twimg.com/media/D-ndAR-U8AEKchO.jpg">

The stories of the victims in the books are disturbing, sad, tragic, dark, haunting, what else does a disaster leave us with other than stories after swallowing the pride, dignity, property, love, and hope. The story of a young wife losing her husband after a year of the wedding, the story of a doctor trying to safely relocate the patients, story of an old lady, the story of the author's parents, etc. According to various sources, the flood took away 500 humans, uncountable lives of animals, pets, birds, insects, and economic loss of 5 Billion USD.


The author makes a comparison of a flood in 19th century which was documented in the colonial rule. The reason is deforestation for the fuel. Development, Development! At various places the history of the city, writings of the various colonial officials, historical battles result, various reports which show the rich heritage of the city and what modern city is missing. Yet the literate, modern(or post-modern) residents are unable to comprehend the importance of maintaining water bodies and urban planning.


Caste in India is a concrete reality, not something present only inside house, villages, towns, burial ground, temple. It's everywhere. As Dr. BR Ambedkar said, caste is a notion in one's mind. The author makes a gripping point on how the marginalized: Dalits, Transgendered, poor day wage laborers suffered in this disaster compared to others. It's a worldwide phenomenon where poor and marginalized who are affected by the natural calamity and results in an increase in poverty, global inequality [2]. The discrimination against Dalits was not new; it happened (2005 Tsunami [3], many more) and happens(2019 Orissa Cyclone incident [4]) in all disasters.


The plight of sanitation workers during the cleanup drive of the city for next week after the flood is inhuman. These sanitation workers were transported from various parts of the state in lorries like carrying livestock, made to stay(>500 workers) in a highly congested wedding hall with only two toilets to use. Almost all of the workers were made to clean up the city without any protective gears like gloves, boots. They were only fed porridge and not proper meals. You may say when the whole city is suffering, how can the officials feed proper meals. How can these people work for long hours without appropriate food? Few of the sanitation workers caught up infections, and some died too. After slogging like a bonded laborer, each one could take away a paltry amount of 50 INR. Does TN recommend minimum daily wage?


Do residents of Chennai recollect those dark days of their life often? Though it may be an event of the past, memories always shape us. Like Haruki Murakami says, "Memories warm you up from the inside, but they tear you apart." While writing this review, Chennai is suffering from water scarcity, do the victims think of those days when water was everywhere refusing to leave?


The author notes down how each and everyone from the city and outside, of whatever size contributed to rescue the victims and rehabilitate. The two communities Fishermen(true even in kerala floods 2018) immensely participated in the rescue efforts of the residents by using various boats, and  Tamil Eelam refugees group helped in restoring the schools, building toilets in the outskirt of the city where very few aids came. The author boldly writes how corporates were only interested in rebuilding the inside city circle under Corporate Responsibility Fund.


The book carries Chennai's map dated in 1914 on the very first page. If the route of the rivers and places mentioned in some chapter had their map in a small form would have enriched the experience of the readers. The readers who never visited or visited a few times who can't recall what is North Chennai, is it north of Mylapore or the places north of  Chintadripet? A meticulous reader will search through this information while reading to build a mental map of the current city.


What's disappointing in the book is a failure to mention the crowdsourced efforts of volunteers under the umbrella [ChennaiRainsOrg](https://twitter.com/ChennaiRainsOrg). There is mention of other volunteering efforts from popular cine actors like Siddarath, RJ Balaji, Surya and Karthik in the chapter title `#ChennaiRains.` 


Rivers existed before the evolution of the human kind; they are source for animals, insects, birds, plants, and various organisms. They run across the city, villages, countries. A single species is destroying it for its whims and fancies and in the name of development. In the countries where citizens treat river as a river, continue to fly boats, enrich them, in the country every river is a holy God, is full of filth, encroachments, dirt. How long can the gross misuse, encroachment, and destruction can go along? An apt question of the time where future generations will ask at us.


Image credits wikimedia.org.

[1]:  https://www.thehindu.com/news/national/tamil-nadu/what-drives-urbanisation-in-tamil-nadu/article7386961.ece
[2]:  https://www.scientificamerican.com/article/natural-disasters-by-location-rich-leave-and-poor-get-poorer/
[3]: https://humanists.international/policy/dalits-and-the-tsunami-2005-05/
[4]: https://www.epw.in/journal/2019/21/commentary/multipurpose-cyclone-shelters-and-caste.html

