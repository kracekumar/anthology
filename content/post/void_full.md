+++
date = "2018-09-19T01:16:21+05:30"
title = "void(full)"
tags = ["free-verse", "musing"]
+++

The presence is not the essence <br/>
The steps aren’t the journey <br/>
The events aren’t the memory<br/>
The wind isn’t the breeze<br/>
The absence of matter isn’t the void<br/>
The out of place presence is the void.