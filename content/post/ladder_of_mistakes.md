+++
date = "2020-06-24T02:22:21+05:30"
title = "Somber HeartBeats"
tags = ["rambling"]
+++

Life - minute after minute mistakes, missteps, and mishaps.
Journey, carrying all pile inside in every step without any spot to unload.
The living is the rent to carry all the untasteful events.

For an external observer, it's an event of failure, for the host, it's a warehouse of faults.
For the host, all events are an unbreakable streak of failures and held by the gravity of failures.
For the observer, it's a chance to learn.

What's there to celebrate on the top of the resentment ladder?

The world as it exists to seed somber heartbeats.

After penning down, all the years of existence, no answer to the question of self-fulfillment.

What drove all the events? The terrible blindness of stupidity.

Who makes life difficult for oneself other than oneself?
