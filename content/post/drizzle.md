+++
date = "2018-10-06T00:50:21+05:30"
title = "Drizzle"
tags = ["free-verse", "musing", "lonely"]
+++

Falling raindrops on the green grass,<br/>
in September,<br/>
on a lazy relaxing Sunday,<br/>
where the weather and the wind are calm.<br/>

The sun is behind the white clouds,<br/>
the warmth is missing in the light,<br/>
the cars racing in the wet roads,<br/>
the houses and the parked vehicles stood frozen.<br/>

Two squirrels running over the lawn,<br/>
to climb the trunks of the chestnut tree,<br/>
leaving the traces of time, and stopped,<br/>
to praise the warmth of the solitude.<br/>
