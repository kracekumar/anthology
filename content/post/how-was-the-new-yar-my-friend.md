+++
date = "2017-12-21T22:22:21+05:30"
title = "How was the year, my friend?"
tags = ["musing"]
+++

    Did you kick-off the year as you desired?<br/>
    Did you get high on new year eve?<br/>
    Did you engross the new year eve with your partner?<br/>
    Did you party hard on new year eve with friends?

Did you spend new year eve traveling?<br/>
Did crackers, and noisy neighbors spoil new year eve?<br/>
Did you spend new year eve at the office?<br/>
Did a book keep you awake on new year eve?<br/>
Did you glare at your text editor on new year eve?<br/>

Did you make new year resolutions?<br/>
Did you remember it now?<br/>
Did the sun shine blossom happiness over the year?<br/>
Did the rains water your soul anytime?<br/>

Did you step in different cities and countries?<br/>
Did you enjoy work?<br/>
Did your bank balance grow?<br/>
Did you prune yourself?<br/>
Did you invest in yourself?<br/>

Did you forget to thank someone for help?<br/>
Did you have belly laugh reading texts?<br/>
Did you lose yourself?<br/>
Did abyss cave inside you?<br/>

Did you turn solitude into tranquility?<br/>
Did you burn inside to feel the warmth outside?<br/>
Did you find what’s burning inside you?<br/>
Did you radiate in the dark?<br/>

Did you get high and felt low?<br/>
Did you find love?<br/>
Did someone love you back?<br/>
Did you break someone’s heart?<br/>
Did you fornicate?<br/>

Did you converse with nature?<br/>
Did you swim in the river?<br/>
Did you stare at the evening sky?<br/>
Did you witness the swarm of evening birds returning to nest?<br/>
Did you feel joyous while returning home?<br/>

Did you deplete all nights alone?<br/>
Did you regret not saying hello to the girl?<br/>
Did you stroll the streets when the city fell asleep?<br/>
Did you find the shadow in the dark nights?<br/>

Did you feel the silence of the city in the midnight?<br/>
Did you understand the city is your companion?<br/>
Did you miss the moon every month?<br/>
Did the moon reflect your feelings?<br/>

Did marriage and relationship status in social media haunt you?<br/>
Did you check out your exes safety during harsh times?<br/>

Did you find your younger self in a stranger?<br/>
Did you find a stranger in you?<br/>
Did you grow older inside?<br/>
Did you strain your relationship with the parents?<br/>

Did you find an album to resonate your thought beats?<br/>
Did you find an author to transport you to another world?<br/>
Did you bond with the dead person’s work?<br/>
Did you acquire a taste for art?<br/>

Did you read a classic?<br/>
Did you find an antagonist in you?<br/>
Did someone's fantasy appeal reality to you?<br/>
Did a character in the book resemble you?<br/>

Did you do a million acts to impress an infant?<br/>
Did you show kindness to infants, outsiders, and minorities?<br/>
Did you feel like an outsider in your birthplace?<br/>
Did you help a stranger?<br/>

Did you lose temper with a stranger?<br/>
Did you treat maids, security guards and others with dignity?<br/>
Did their life improve last year?<br/>
Did their children believe in future?<br/>

Did you think of life in other parts of the world?<br/>
Did you think about the life of have-nots?<br/>
Did your belief system change?<br/>
Did your thoughts waver every month?<br/>
Did your strong opinions become weak?<br/>

Did you feel peace at heart?<br/>
Did you date yourself?<br/>
Did kafkaesque dream scare you?<br/>
Did you wake up to Aadhar texts?<br/>

Did a death move you?<br/>
Did you friend an infant in real life?<br/>
Did you encounter a human who changed your thinking and feeling?<br/>
Did another human’s happiness lit your heart?<br/>
Did you shed tears for an unknown human?<br/>

Did you get time to think about every month events?<br/>
Did you revisit your memories?<br/>
Did you relook at your yesteryear snaps?<br/>
Did you think about your childhood?<br/>
Did you picture yourself in future?<br/>

Did you steer the year in your direction?<br/>
Did you experience the year was a repeat of the previous year?<br/>
Did your will to live change?<br/>

Did you find a reflection of yourself elsewhere?<br/>
Did you contemplate on life?<br/>
Did you begin the quest for the meaning of life?<br/>
Did you find logic in the string of absurd life events?<br/>
