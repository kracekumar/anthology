+++
date = "2018-06-08T00:52:21+05:30"
title = "Twilight Sky"
tags = ["free-verse", "musing", "memories"]
+++

When the sun is down,<br/>
The moon is still invisible,<br/>
The sky is full of afterglow,<br/>
The lone star shining in the air,<br/>
A different new light is yet to seep in,<br/>
Musing darkness is around,<br/>
The big things are yet to happen,<br/>
Small things playing big,<br/>
And pushing forward,<br/>
Like raft stroke in the vast river,<br/>
And everything else is happening <br/>
To reveal it’s color,<br/>
Hang on to the bright thing<br/>
Irrespective of size and impact, <br/>
Be in the moment<br/>
And capture all of it,<br/>
That’s how moments are made,<br/>
Memories grow.<br/>
