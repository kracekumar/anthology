+++
date = "2018-09-19T01:10:21+05:30"
title = "Our Superhero"
tags = ["free-verse", "musing"]
+++

Our heroes don’t kill enemy troops<br/>
They safeguard and rescue our lives during the disaster.<br/>

Our heroes don’t defend the border <br/>
They roam free in the endless mass of water.<br/>

Our heroes don’t wear uniform and cap,<br/>
They are humans of color and wear lungi.<br/>

Our heroes don’t defend with guns,<br/>
They save lives with the raft.<br/>

Yet folks called them untouchables, and thieves,<br/>
Oh, yes, they are untouchables because,<br/>
They are the children of the selfless mother,<br/>
They never witness borders, <br/>
Their mother’s wealth is immeasurable,<br/>
Their mother never let’s visitor return with empty hands,<br/>
They don’t fear ten feet vigorous current,<br/>
They don’t fear snakes floating in the flood, <br/>
They are unbribable,<br/>
They don’t bend before power,<br/>
They bend down to rescue citizens,<br/>
Oh, my country fellow, you’re a dust peek in their mountain of humanity and bravery.
<br/>
Our superheroes are fishermen.
