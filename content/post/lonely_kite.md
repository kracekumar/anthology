+++
date = "2018-08-06T00:50:21+05:30"
title = "Lonely Kite"
tags = ["free-verse", "musing", "lonely"]
+++

The dark clouds roared,<br/>
The breezy wild wind <br/>
blew the earth and raised the debris.<br/>
The tree swung in all directions,<br/>
The lightning cut across the sky,<br/>
Birds all flew to a safe place,<br/>
And humans, dogs ran to shelter.<br/>

I stood alone in the balcony,<br/>
With a cup of green tea,<br/>
Immersed at the moment.<br/>
Looked around to share the joy of rain,<br/>
Finding an empty balcony<br/>
And realized I’m a lone kite lost in the sky.<br/>