+++
date = "2018-12-09T23:41:21+05:30"
title = "The Second Sunrise"
tags = ["free-verse", "musing"]
+++


As soon as you wake up,<br/>
a message on the phone screen flashes,<br/>
“How was the weekend?”.<br/>
In a minute, you recollect,<br/>
the vibrant scenes of the weekend,<br/>
and once again experience the<br/>
two days while still yawning with<br/>
half eye closed.<br/>
<br/>
The question brings a whole light to the Monday,<br/>
like a second sunrise in the day,<br/>
And like a summer river joyously flowing<br/>
after heavy showers,<br/>
washing all the reminiscences in the course,<br/>
bring the unexpected sediments.<br/>