+++
date = "2018-11-24T23:23:21+05:30"
title = "Working to escape feelings"
tags = ["free-verse", "musing"]
+++
<br/>
When the new year sun shines on the grass,<br/>
it’s a next number in the common era,<br/>
It is a continuation of yesterday.<br/>
<br/>
The biological aging process continues,<br/>
Deaths increases,<br/>
Deceased don’t wake up from the coffin,<br/>
And sit in the weary stone bench<br/>
And contemplate is time linear in the earth?<br/>
<br/>
On a sunny afternoon,<br/>
In the middle of a traffic jam,<br/>
Tens of bots, and automatons,<br/>
send you the birthday wishes.<br/>
Those senders don’t mean anything,<br/>
Like how you don’t mean anything to anyone.<br/>
<br/>
The raindrops falls from clouds,<br/>
replaces the noise with thudding sound,<br/>
cools down the earth, and <br/>
leaves an empty feeling inside.<br/>
<br/>
Even on a solo date,<br/>
it’s a sugarless coffee in the cafe,<br/>
the jukebox still can’t hit the feeling,<br/>
And it’s still a gloomy day inside.<br/>
<br/>
Does it matter what day of the week it is?<br/>
Does it matter what holiday it is?<br/>
Does it matter what season it is?<br/>
Does it matter which month it is?<br/>
Does it matter who calls you up?<br/>
<br/>
There is one thought always, how can I repress this feeling?<br/>
Overusing the reason,<br/>
picking up tasks to keep the mind occupied,<br/>
Like a dog trying to bite its own tail.<br/>
<br/>
Where can I find the peace?<br/>
When you fight against your own feeling,<br/>
solitude and darkness are the allies.<br/>
The dreams don’t spare you,<br/>
The specter haunts every day.<br/>
<br/>
How long can I shove the feelings?<br/>
Even when I bury it in the deepest ocean,<br/>
the waves carry it back to the shore,<br/>
Why don’t tears evaporate,<br/>
and leave me empty?<br/>
<br/>
The words paint an  image in mind,<br/>
to create an experience.<br/>
A long forgotten word’s meaning disappears <br/>
from memory,<br/>
but a long-neglected feeling never leaves the body.<br/>
<br/>
It’s been long, I lost track of the clock,<br/>
No, where I can see the land,<br/>
Does inter-continental migratory birds,<br/>
feel the pain in the wings?<br/>
<br/>
How long is the war?<br/>
All shots are without shedding blood,<br/>
but not without scars and wounds,<br/>
and who waged it on me?<br/>
Is there a bunker to hide?<br/>
When I fall on the battleground<br/>
will I be called martyr?<br/>
Is there anyone who will identify the corpse?<br/>