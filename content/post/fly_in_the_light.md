+++
date = "2018-09-19T01:11:21+05:30"
title = "Fly in the night"
tags = ["free-verse", "musing"]
+++

I fly in the night<br/>
Like the bird questing for the nest.<br/>

I see the dispersed clouds in the blue sky<br/>
Like the freedom to roam.<br/>

I collect the scraps in the street<br/>
Like cleaning the heart.<br/>

I fell asleep and woke up<br/>
Like a security guard in the gate.<br/>

I feel I found a purpose in life<br/>
Like the howling street dog in the night.<br/>

I thought I lived life <br/>
Like a forgotten lighthouse in the harbor.<br/>
