+++
date = "2018-03-24T1:42:21+05:30"
title = "Review: Existentialism is a humanism"
tags = ["book-review"]
+++
Existentialism is humanism is a book compiled out of Jean Paul Sartre lecture. In the talk, Sartre answers questions
- What is existentialism?
- What are the principles of existentialism?
- What does existentialism teach human?
- Does existentialism disseminate quietism and despair?
- Who is human?
- What is life?
- What is freedom?
- Does presence or existence of God have an impact on human life?
- And other seminal questions on the topic.

The book also contains a review of the book, The Stranger by Albert Camus's philosophy, Absurdism.

Following are the critical points for the lecture

### What is Existentialism?

Existentialism is a doctrine that makes human life possible and also affirms that every truth and action imply an environment and human subjectivity. In a classic way to quote, “existence precedes essence”.

What does that mean? When a human considers producing a paper knife, the human drew the inspiration from a concept, paper knife, and formulates the production technique, and manufactured in a certain way. The creation serves a purpose. So the essence of the knife precedes its existence since the paper knife is the sum of formulae and its properties enabled to produce the paper knife.

Taking this example further, human first conceives oneself only after human exists; then the human is thrown into the existence, the human is nothing but what the human makes out of oneself.

### Who is Human?

Human is a project that has a subjective existence. Before projecting of the self, nothing exists, not even intelligence, and human shall attain existence only when human projects self to be.

The human exists only to exists only to the extent that human realizes the self. Human is no more than the sum of the actions in their life.

### What is the effect of existentialism?

The effect of existentialism is to make every human conscious of what human is and to make solely responsible for the existence.

Further, Sartre makes important points and raises questions like who chooses humanity - it’s not enough for the public to speak of humanity, since legislator’s also decides what humanity is, how do you distinguish angel and disguised human, how do you confirm the sign is from heaven  etc.…

### Freedom

The human is condemned to be free: condemned because did not create himself, yet nonetheless free, because once cast into the world, the human is responsible for everything human does. If God exists human is not free.

You’re free, so choose, in other words, invents. No general code of ethics can tell you what you ought to do; there are no signs in the world.


### Life

Life itself is nothing until it is lived, it is we who give meaning to it, and value is nothing more than the meaning that we give it.

Existentialism never considers human as an end, because human is constantly in the making.

### Commentary on the stranger

Sartre’s starts of the review of the book with a comparison of the central character, Meursault with other humans of the real life like moral vs. immoral, and the explains why any of these comparisons don’t fit the character. He moves on to compare the dialogues the Meursault utters, his actions, behavior, mockery of daily routine, what love means to him, his feeling, throughout the novel.

By emphasizing the difference of the narrator’s character presents an opportunity for the readers to re-evaluate the understanding of the underlying message of the novel. Sometimes, the readers overlook the technicality of these dialogues.

### Sartre answers who is an absurd man?

The absurd man will not commit suicide; he wants to live without relinquishing any of his certainty, without a future, without a hope, without an illusion, without resignation, either. The absurd man asserts himself by revolting. He stares death with passionate attention, and this fascination liberates him.

Sartre is in awe of Camus's style of writing, depiction, narrator’s voice throughout the novel, shaping up the character and provocative feeling of the dialogues. In the end, he places the work alongside Voltaire’s Candide.
