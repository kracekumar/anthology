+++
date = "2018-12-10T00:15:21+05:30"
title = "No one rings the door bell"
tags = ["free-verse", "musing"]
+++
<br/>
The house is clean, fragrant,<br/>
the refrigerator is full,<br/>
the snack jar overflows with nuts,<br/>
three single malt whiskeys in the kitchen shelf,<br/>
fresh farm fruits in the basket.<br/>
<br/>
The day and night, I prepare food, <br/>
And feed me watching videos.<br/>
Other than online order delivery boys,<br/>
No one rings the doorbell.<br/>