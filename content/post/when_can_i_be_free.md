+++
date = "2018-11-24T11:28:21+05:30"
title = "When can I be free?"
tags = ["free-verse", "musing"]
+++
<br/>
<br/>
When I'm in the top of the mountain?<br/>
When I leave all my property in the flat land?<br/>
When my parents die?<br/>
When there is nothing to chase?<br/>
When am I alone for years?<br/>
When I’m about to die?<br/>
When residing in a strange country?<br/>
Oh, we’re all condemned to be free.<br/>

