+++
date = "2018-06-03T22:35:21+05:30"
title = "Two chairs"
tags = ["free-verse"]
+++
On a mild summer afternoon,<br/>
I stood on the balcony leaning on the railing.<br/>
As I looked around all the balconies in the apartment,<br/>
In the east side, in a gallery,<br/>
Two empty chairs lay amidst of the flower pots.<br/>

A few minutes later,<br/>
A feral pigeon sat in the chair,<br/>
And another pigeon flew into the balcony,<br/>
Landed in the chair.<br/>

Both looked around <br/>
Looked up, turned the neck sides.<br/>
One of the pigeons flew and sat on the other chair.<br/>
Two pigeons sat in the single chair<br/>
And gazed the trees, birds, and the realm.<br/>

A couple slide open the balcony,<br/>
Pigeons flew and sat on the railings.<br/>
Each wore a minimal dress,<br/>
Sat on the chair gazing the sky.<br/>

The blue sky with motionless patchy clouds,<br/>
A crow circled the sky leisurely,<br/>
They looked at the trees, <br/>
Spoke about all mundane things,<br/>
Looked around aimlessly,<br/>
Enjoyed the view, sun,<br/>
And tried luring pigeons.<br/>

Two pigeons flew away from the balcony<br/>
And sat on the faraway tree trunk.<br/>
As the moments passed, <br/>
Both the hearts spoke in the silence.<br/>

Once the pigeon flew away from the tree,<br/>
Couples moved into the house,<br/>
Contours of the love were<br/>
Visible from my balcony for eternity.<br/>
