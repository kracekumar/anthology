+++
date = "2019-05-25T23:00:00+05:30"
title = "2019 elections infographics"
tags = ["elections"]
+++

### General Elections

- Whom did TN electors vote for in 2019 general elections?

DMK+: 52.70%, ADMK+: 30.10%, AMMK: 5.13%, NTK: 3.89%,
MNM: 3.75%, NOTA: 1.27%, OTHERS: 3.15%. In Box plot, AMMK and MNM have outliers ignoring 'OTHERS.'
MNM's main vote share(>10%) came from four constituencies Chennai (3) and Coimbatore (1). 

![Example image](/images/2019 elections/general_elections_party_vote_share_pie_chart.png)
![Example image](/images/2019 elections/general_elections_party_vote_share_box_chart.png)

- Constituency wise all parties vote share.

![Example image](/images/2019 elections/general_elections_all_constituencies_party_wise_vote_share_barh_plot.png)


- What are the constituencies were winning margin is less than 10%? 

Five constituencies. 
Chidambaram (VCK):  0.28%, Dharmapuri (DMK): 5.79%, Theni (ADMK): 6.54%, and Tiruppur (CPI): 8.34%.

![Example image](/images/2019 elections/general_elections_winning_percentage_less_than_10_perc_barh_plot.png)

- AMMK, NTK, MNM, and NOTA vote share across all constituencies in general elections.

![Example image](/images/2019 elections/general_elections_ammk_vote_share_all_cons_barh_plot.png)
![Example image](/images/2019 elections/general_elections_ntk_vote_share_all_cons_barh_plot.png)
![Example image](/images/2019 elections/general_elections_mnm_vote_share_all_cons_barh_plot.png)
![Example image](/images/2019 elections/general_elections_nota_vote_share_all_cons_barh_plot.png)

### Bye Elections

- Whom did TN electors vote for in 2019 bye-elections(22 constituencies)?
DMK+: 44.91%, ADMK+: 38.17%, AMMK: 8.28%, NTK: 3.13%,
MNM: 2.50%, NOTA: 1.06%, OTHERS: 1.94%. In Box plot(second image), MNM has outliers ignoring 'OTHERS.'

![Example image](/images/2019 elections/bye_elections_party_vote_share_pie_chart.png)
![Example image](/images/2019 elections/bye_elections_party_vote_share_box_chart.png)

- Constituency wise all parties vote share.

![Example image](/images/2019 elections/bye_elections_all_constituencies_party_wise_vote_share_barh_plot.png)

- What are the constituencies where winning margin is lesser than 10%? 
Nine constituencies. Sulur(ADMK):  4.46%, Thirupparankundram(DMK): 1.06%,Paramakudi (SC) (ADMK): 7.91%, 
Sattur (ADMK): 0.60%, Andipatti (DMK): 6.07%, Manamadurai (SC) (ADMK): 4.17%,
Harur (SC) (ADMK): 4.78%, Pappireddipatti (ADMK): 8.59%, Sholinghur (ADMK): 7.44%.

![Example image](/images/2019 elections/bye_elections_winning_percentage_less_than_10_perc_barh_plot.png)


- What is the DMK's difference of votes in losing constituencies?
Sholinghur: 7.44%, Pappireddipatti:8.59%, Harur (SC): 4.78%,Nilakottai (SC): 11.23%, Manamadurai (SC): 4.17%, 
Sattur: 0.60%,Paramakudi (SC): 7.91%,Vilathikulam:,Sulur: 4.46%.

![Example image](/images/2019 elections/bye_elections_dmk_losing_constituency_barh_plot.png)


- AMMK, NTK, MNM, and NOTA vote share across all constituencies in bye-elections.

![Example image](/images/2019 elections/bye_elections_ammk_vote_share_all_cons_barh_plot.png)
![Example image](/images/2019 elections/bye_elections_ntk_vote_share_all_cons_barh_plot.png)
![Example image](/images/2019 elections/bye_elections_mnm_vote_share_all_cons_barh_plot.png)
![Example image](/images/2019 elections/bye_elections_nota_vote_share_all_cons_barh_plot.png)

Source collected from election comission site: https://docs.google.com/spreadsheets/d/1ll4X8NHrF4SkGQ2IH_ESZFJbKD7ImK3PquoMD2iL518/edit?usp=sharing
Analysis code: https://nbviewer.jupyter.org/gist/kracekumar/48ea87d3daada0ab6a567c80bfae4a98/Analysis.ipynb