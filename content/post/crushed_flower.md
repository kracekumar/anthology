+++
date = "2018-08-06T00:40:21+05:30"
title = "Crushed Flower"
tags = ["free-verse", "musing"]
+++

The stolen flower smells same in a seller’s, child’s and a dictator’s nose, <br/>
The crushed petals carry the taste of the flower remains in the syrup,<br/>
When circumstance crushes humans and time washes away our possession,<br/>
What stays with humans?<br/>