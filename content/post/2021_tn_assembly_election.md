+++
date = "2021-05-09T15:00:00+05:30"
title = "2021 Tamil Nadu Assembly Election Result Analysis"
tags = ["elections", "tamilnadu"]
+++

In 2021 Tamil Nadu assembly elections, DMK alliance won the 159 seats out of 234 and AIADMK
alliance won 75 seats and Stalin became Chief Minister of TamilNadu. The post analysis the
results of the election based on votes, parties and alliances performances.

# Vote Share


- DMK Alliance had a lead over AIADMK alliance by `6%`.

- DMK+: 45.6%
- AIADMK+: 39.5%
- NTK: 6.57%
- AMMK+: 2.87%
- MNM+: 2.75%
- NOTA: 0.75%
- Others: 2.01%.

![Alliance Vote Share](/images/2021 elections/alliance_vote_share.png)

- In 2016 assembly election, NOTA secured 1.1% of votes, this time `NOTA` received `0.75%` of votes.

- The majority of the postal votes was secured by DMK alliance. Total valid postal votes were `4,89,699`.

- DMK+: 62.13%
- AIADMK+: 28.3%
- NTK: 4.01%
- AMMK+: 2.87%
- MNM+: 2.75%

![Postal Vote Share](/images/2021 elections/postal_vote_share.png)

- Postal votes helped two candidates win the election in DMK alliance Durai Murugan in Katpadi and Palani in Tenkasi. Both were trailing with EVM votes.

![Postal Vote Share](/images/2021 elections/postal_votes_exclusive_votes.png)

- DMK alliance party wise vote polled percentage.

- DMK: 46.67%
- CPI: 43.78%
- VCK: 42.58%
- INC: 41.53%
- IUML: 38.63
- CPI(M): 35.97%

![DMK Alliance Votes Polled](/images/2021 elections/dmk_alliance_partner_vote_share.png)

- AIADMK alliance party wise % votes polled.

- AIADMK: 40.23%
- PMK: 37.65%
- BJP: 34.46%

![AIADMK Alliance Votes polled](/images/2021 elections/admk_alliance_votes_share.png)

- Out of 6 seats, VCK won 4 seats with an average of 42.58% of votes in each seats. Out of 4 winning seats, two were in
`General` and two were in `SC` constituency types.

![VCK Votes](/images/2021 elections/vck_votes.png)

# Independent Candidates

- Seventeen candidates contested in each constituency, and 77 candidates competed in the Karur constituency and highest in the election.

![Top 10 Contested Seats](/images/2021 elections/top_10_contested_seats.png)

- Only two independent candidates polled double-digit vote percentage and one secured deposit. Hari polled `18.54%` of votes(37,727) in the `Alangulam` constituency, and Bala Krishnan secured `13.41%` of votes(23,771).

![Top 10 Independent Candidates](/images/2021 elections/top_10_independent_candidates.png)

# Women Candidates

- `405`, (8.35%) women candidates contested in this election. Twelve women candidates won - `seven` from reserved constituencies and `five` from the general constituency.

- DMK - 6, AIADMK - 3, BJP -2, and INC - 1.

![Women Candidate Winners](/images/2021 elections/women_winning_candidates.png)

- Vanitha Srinivasan from BJP in `Coimbatore South` secured lowest percentage of votes, `34.48%`.
- Vijaya Dharani from INC in `Vilavancode` secured highest percentage of votes, `52.12%`.

- ADMK and DMK alliances fielded women in 32 constituencies. In those 30 constituencies, the number of women participation was above 8.33% (average in this election). The highest women participation was in `Gudiyatham - 46.27%`, six women and seven men.

![Women participation District wise](/images/2021 elections/district_wise_gender_distribution.png)
![Women participation District wise](/images/2021 elections/district_wise_gender_distribution_candidates.png)

- The participation of women candidates significantly goes up when recognized parties field women candidates. 36 out of 383 candidates in the Chennai district were women candidates.

- `PMK(1/23)` and `INC(1/25)` each fielded one woman candidate. PMK candidate, `Thilagabama` in `Athoor` scored `13.5%`, and Vijaya Dharani in Vilavancode secured `52.2%`. Independent women and men scored a similar percentage of votes except for Hari N in Alangulam.

![Party wise women candidate vote percentage](/images/2021 elections/partywise_gender_candidate_vote_dist.png)

- `Reserved constituencies(16%)`  witnessed twice as many candidates as women candidates than general constituencies(8.3%). NTK fielded 80% of women in the reserved constituencies. (note: the image count does not add to 117 for women candidates).

![Women Candidate by Constituency Type](/images/2021 elections/women_candidate_constituency_type.png)

- Apart from NTK(50%-50%), the ADMK alliance fielded nine women candidates than six from DMK in SC constituencies. The ADMK alliance field 11 women candidates compared to 7 women candidates in general constituencies. In ST constituencies, ADMK fielded one woman candidate, and NTK fielded two women candidates.

# Close fights

- `32` seats were closely fought - 2% victory margin, 32 seats - `13.67%` seats. In 8 seats, the winning margin was less than 1000 votes (< 0.6%). `Thiyagarayanagar: 137, Modakkurichi: 281, Tenkasi: 370`.

![Tough seats](/images/2021 elections/tough_seats_margin_votes.png)


- Out of 32  seats, `DMK+` won on 17 seats. Out of 10 constituencies in Coimbatore, five constituencies were closely fought. ADMK+ won all the seats. Out of five constituencies in Tenkasi, three were closely fought, and ADMK+ won one and DMK+ won two seats.

![Tough Seats by District](/images/2021 elections/tough_seats_by_districts.png)

![Tough Seats votes by party](/images/2021 elections/tough_seats_party_wise_votes.png)

- DMK alliance didn't win a single seat in `Dharampuri(3 seats)` and `Coimbatore districts(10 seats)`. In Dharampuri, ADMK+ had a lead of `13.61%` votes, and in Coimbatore, 6% lead. MNM+ scored 10.55% in the district(4 times higher than party average and highest for the party in any district).

![District wise alliance votes](/images/2021 elections/district_wise_alliance_vote_avg.png)

- Out of 38 districts, DMK+ scored less than 45% votes in 13 districts. Sivagangai district, NTK, and AMMK+ scored 11.2% and 11.99%, respectively, and DMK won two out of three seats with 38.28% of votes - the only district where these parties secured their highest vote share.
![Districts where dmk score less than 45%](/images/2021 elections/district_wise_vote_share_when_dmk_is_less_than_45.png)

- `AIADMK+` polled 2% higher than average in reserved constituencies. NTK's scored slightly(0.3%) higher than their average in reserved constituencies. MNM polled 3.23% in general constituencies compared to scheduled caste constituencies, 1.43% of votes.
![Constituency type wise vote share](/images/2021 elections/consituency_type_wise_alliance_share.png)

# NTK

- Let's move on to NTK voting patterns(6.5% to 7%). Six out of the top ten NTK candidates with the highest votes are women. After Seeman, Velraj secured the highest % of votes, 16.42(deposit= 16.5%) in Thothukudi.
![Top 10 NTK candidates](/images/2021 elections/top_10_ntk_canddiates.png)

- What's their avg vote against the winning party's constituencies? When the INC wins a seat, NTK gets their best average - 15286 and lowest when PMK wins - 9153. When DMK wins, it's 13600 and 11689 when AIADMK wins.
![NTK against winning votes](/images/2021 elections/ntk_winning_avg_votes.png)

- NTK performs better against CPM(7.75%) and INC(7.44) - 1% higher than the average of 6.5% and performs poorly against PMK - 5.93%.

![NTK against DMK](/images/2021 elections/ntk_vote_shared_against_dmk_plus.png)
![NTK against AIADMK](/images/2021 elections/ntk_vote_share_against_admk_plus.png)

- NTK's top three districts for votes are - Sivagangai, Thiruvallur, and Thotthukudi, and the lowest vote-getting districts are Dharmapuri, Krishnagiri, and Salem.
![NTK District wise vote](/images/2021 elections/district_wise_ntk_votes.png)

# Victory Margin

- In 32 constituencies, AMMK secured more than 5% of votes. In twenty constituencies(> 2% diff), AMMK votes scored more votes than losers' differences. DMK alliance won in 14 constituencies, and the ADMK alliance won 6 constituencies.
![AMMK Votes](/images/2021 elections/ammk_gth_5pct.png)
![AMMK Votes Mattered](/images/2021 elections/ammk_vote_mattered.png)
![AMMK Votes Helped](/images/2021 elections/ammk_votes_helped_table.png)

- In 52% of constituencies, the DMK alliance won with more than a 10% margin. 62% of seats(46/75) ADMK alliance won in less than 10% margin.
![ADMK Victory Margin](/images/2021 elections/admk_victory_margin.png)
![Winning Party Victory Margin](/images/2021 elections/winning_party_victory_margin.png)
![DMK Victory Margin](/images/2021 elections/dmk_victory_margin.png)

- Nine out of 10 most significant victory margin by percentage votes belongs to the DMK alliance. In ADMK, only Edappadi's victory was the biggest, with a 37.93% margin.
![Big Margin](/images/2021 elections/top_10_victory_margin_by_pct.png)

- MNM secured 10.55% of votes in Coimbatore district, followed by `8.55` in Chennai.
![MNM Votes by district](/images/2021 elections/mnm_votes_by_district.png)

- Kamal Hassan and Dr. Mahendran secured deposits from MNM. Top three most votes came from the Coimbatore district.
![MNM Candidate Votes](/images/2021 elections/top_mnm_candidate_votes.png)

### Note:

1. There may be a discrepancy with other data sources like Wikipedia or media outlets.
2. In the dataset, two NTK constituency candidates are marked as male rather than marking them as female based on the claim 50% male candidates and 50% Female candidates.


### References:

1. [ECI Result Website - Data Source](https://results.eci.gov.in/Result2021/partywiseresult-S22.htm)
2. [Myneta women candidates](https://myneta.info/TamilNadu2021/index.php?action=summary&subAction=women_candidate)
3. [kaggle](https://www.kaggle.com/kracekumar/tamilnadu-assembly-election-2021)
4. [GitHub Repo](https://github.com/kracekumar/Tamil-Nadu-Assembly-Election-2021)
5. [Twitter Thread](https://twitter.com/kracetheking/status/1391144584663601153) with all the data analysis.
