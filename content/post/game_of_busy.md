+++
date = "2018-04-22T22:35:21+05:30"
title = "Game of busy"
tags = ["free-verse"]
+++

A press of a button sends a message<br/>
In a second message is delivered<br/>
We play the game of busy<br/>
And make another human yearn<br/>

After reading the message<br/>
Reply instantly pops out in the mind<br/>
But we wait for long enough<br/>
To demonstrate the busy<br/>

Though in the interval <br/>
Nothing changes inside us <br/>
And the ticking time outside<br/>

For the sake of waiting <br/>
We create an artificial silence<br/>

Why does it at all?<br/>
Can’t we just be genuine<br/> 
And show the interest <br/>
Dislike and move on?<br/>

When the medium was slow<br/>
Love grew organically <br/>
When the medium was quick<br/>
We added lag<br/>

Let’s be true and show desperation<br/>
And acknowledge the missing love<br/>
Let’s bind together before biases<br/>
Take over the feelings<br/>

