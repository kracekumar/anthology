+++
date = "2018-11-24T23:06:21+05:30"
title = "Leaving no trace"
tags = ["free-verse", "musing"]
+++
 
The daily food was tasteless,<br/>
It was edible and not a waste,<br/>
Then I stuck to fruits,<br/>
And it shook the juice inside me.<br/>

The vision was clear with an aid,<br/>
I never saw a place to land,<br/>
Everyone strolled hand in hand,<br/>
The crowd moved with a dread,<br/>
Without a final destination to end.<br/>

The words were all in the space,<br/>
Traveled out of place,<br/>
All the words were in my mind,<br/>
I lived in an echo chamber.<br/>

I cut the connection with the virtual world,<br/>
The persona faded like daylight,<br/>
All the connected night lives lit the eyes,<br/>
No one could spot the missing.<br/>

After all the accounts disappeared,<br/>
I was out of their memory,<br/>
And I fell in the dark forest,<br/>
No one found me missing,<br/>
I left without a trace.<br/>

