+++
date = "2019-07-15T00:25:21+05:30"
title = "Where India Goes Book Review"
tags = ["book review", "caste", "health", "sanitation"]
+++

As of 2017, 17.9 % of the world population lives in India. 60% of the global population lives in Asia; 16 % of people live in Africa. As the population grows in India, the IMR(Infant Mortality Rate) is slowly dropping. Out of 190 countries, India occupies the 125th place with IMR rate of 41.36(as of 2015), out of 1000 babies born, 40 babies die in the first five years. These are facts or undeniable truths. What are the primary factors for high IMR? It's linked with baby's nutrition, mother's nutrition, access to health care, and the surrounding environment.

<img src="https://pbs.twimg.com/media/D-u3LoaUYAA9aWq.jpg">


The book "Where India Goes"  focuses on one of the neglected topic, open defecation. The book shows the totality of the issues by structuring the book into three parts: Causes, Consequences, and Responses. The authors Diane Coffey and Dean Spears had employed various techniques to measure, compare practices recommended by International bodies like UNICEF, WHO across the globe. By collaborating with multiple experts like economists, the authors demonstrate how poverty is not the only issue why people practice open defecation. In contrast, poor countries like Bangladesh, Nepal, sub-Saharan countries lower open defecation rates.


### Causes: Why do people open defecate?


Most people believe poverty is the only reason why people defecate in the open. It's an easy, logical observation. If poor people had enough money, all would build better houses with latrine and start using it. It's valid to a certain extent but not for all. People who live in non-concrete houses, as pointed out in the book, consider it as a lavish possession. When the entire family lives in a single room house made out of mud and cow dung, the toilet is an expensive investment with no return.


Even today, after a hair cut or after attending a death ritual, religious Hindu families insist individual to take a bath outside the home and enter.  Leave the worn clothes outside the house, change the dress, and enter the house. Before taking a shower, an individual is supposed not to touch any other individual who hasn't accompanied them. Performing death rituals or hair cut was considered the ritually destructive task. Hence the purity. Today, a lot of household practice similar ritual purity towards women who menstruate.


In this book, authors had made exhaustive studies and interviews in various states: Uttar Pradesh, Bihar, Madhya Pradesh, Rajasthan, and Harayana. A lot of people who practice open defecation in rural areas pointed out the ritual impurity of having a toilet next to the living house. Even when the new homes in rural areas with toilets, the household still practice open defecation. They are used to the habit of waking up early, defecate twice in the open: before sunrise and after sunset. A solid one hour or more spent in discovering the place, walking to and from the house in the woods. Most people still view this a hygienic task of breathing fresh hair, flexing the muscles, spending time in nature. Defecating in the open may feel romantic and nature-friendly. But these are the unaware people about germs and how it affects the child growth, stunt development, health of the mother, and child's cognitive development.


The rate of open defecation is rural is as close as 70%, which is alarmingly large. The current census of India only captures whether the household possesses a toilet or not, whereas the survey conducted by the authors' team clearly shows even when the house possess a toilet, in a large number of families, one of the members still practice open defecation. The SQUAT survey data undertaken by the authors' team found at least one person in 40% of the house who possess a toilet still defecate in the open.


Various state and union governments under different initiatives promote the construction of subsided toilet to households and villages, yet people prefer to defecate in the open among the high school educates and with sufficient water available in the houses and public toilets.


A sharp observation by the authors, "In obsession with ritual purity and natural way of living, we compromise on physical cleanliness."


### Cost of toilet construction


In India, the toilet construction follows standard practice, a ground cement tank to collect the excreta, a cement structure with walls above the ground, mostly with a roof, cement floor, basin in the floor and an underground pipe connecting the basin and collection tank. The cost of the construction varies on the size of the toilet room and depth of the cement tank. On average, the installation costs 21000 INR. The Swacch Bharat Mission promotes the construction of a toilet with a cost of 12000 INR with shorter collection cement tanks.


The many other toilets around the world cost 2000 INR to 3000 INR. These are called pit latrines recommended by WHO. A pit is dug in the underground below the basin. Excreta falls into the hole. It takes six months for human excreta to decompose, the pit is cleaned up after decomposition. These pit-latrines are common in Bangladesh and other African nations. Due to the myth of quick filling of pits and cleaning the pits, most of the household avoid this kind of construction. In many places, people were unaware of these available options. The volume of Indian collection tanks is sevens times higher than the WHO recommended size.


### Untouchability, Caste, and Manual Scavenging


The law forbids manual scavenging, yet the social practices continue. Every year thousands of them die, few states report them wholly and partially, and a few others don't report. Since 2013, governments started outsourcing these jobs to contractors; as a result, governments declare in the records as zero manual scavengers employed. When news media carry a sanitation employee death while cleaning a septic tank, most media houses report them as an accident. There is a 100% reservation for oppressed castes when it comes to manual scavengers.


In a lot of areas, people defecate in concrete slabs; later manual scavengers clean the excreta by spreading the ashes and dispose the excreta outside the city or village. With the availability of cheap labor, societal injustice towards Dalits, the practice of open defecation in dry latrines continue. The inhuman task of collecting a human excreta by a human without any protective gear imposed by birth continues to prevail throughout India with the daylight support of society and governments. Activist Dhivya Bharathi made a documentary film on the topic, manual scavenging focusing on Tamil Nadu and it's inhuman practice based on caste hierarchy and plights, prejudices against Dalits. The documentary is available on YouTube under the title [Kakoos](https://www.youtube.com/watch?v=-UYWRoHUpkU).


Even today the contractors or self-employed individual who cleanup filled septic tanks using the machine are from oppressed castes. Either they are Dalits now or who converted to various religions. Even there is demand for these type of tasks, yet no other castes or caste-neutral entrepreneurs are willing to set up a business unit. The answer is simple and straight forward: the stigma associated with doing menial jobs and caste supremacy. Of course, capitalists will never equate this to supply and demand.



### Why is it a problem for everyone?


The fascinating fact of the authors' survey is the difference in IMR and open defecation rates between Muslims and Hindus in India. On average, Muslims in India are poor compared to Hindu counterparts. The IMR amongst Muslims is 74 and among Hindus is 88(in five states). The authors examine a similar situation between West Bengal and Bangladesh, where more impoverished neighbor outperform in IMR and open defecation rate.


Another reveling correlation is height. The countries where open defecation is lower, the height of the average population is higher. For example, a lot of Indians assume Japanese are shorter. But the facts establish something else, on the average Japanese are taller than Indians by eight cms. Europeans tend to be taller in the world. The Africans are taller than Indians. The open defecation affects the height of the individual and the surroundings exposed to it. Most of the time, genetics is the resort about height. There are interesting observations from various studies; Indians who migrate to different Scandinavian countries are taller than their parents and Mayans who emigrated to the USA from Guatemala were found taller than their countries average.


In 2015, 57.1% of Indians defecated in open compared to 12.1% in Bangladesh, according to UNICEF and WHO. Despite the economic disadvantage, the Bangladesh population is taller than the average West Bengal population.  More Women in Bangladesh can read compared to Indian counterparts, economically freer, and able to raise their children in better surroundings.


The workforce where open defecation is lower earns higher wages. The students whose family and surroundings open defecate have seen to suffer from health issues which internally affects cognitive developments. Healthier the child, attentive it's in the class. Sickness leads to missing the class. Open defecation makes children shorter and globally on average, taller population earns more income. The case studies shown are from the USA and Mexico. Also, rich people tend to be taller.


### How the government and policymakers view this?


Government has it's own propaganda machinery. Government offices are full of racks and piles of forms, collected information, and statistical data. Swacch Bharath Mission started on 15, August 2015 launched by prime minister Narendra Modi, includes one of the goals as to eliminate open defecation by 2019. As of 2014, open defecation was around 80%. The current rate of decline is 1% in India. The target is ridiculously put forward and nowhere on the globe; a country had wiped off open defecation in a mere five-year term. The fastest has been Ethiopia with a rate of decline is 17% as the authors note.


The authors rightfully note SBM promised to build, 12.3 latrines in the 5-year term, which is a rate of 67000 new toilets per a day, close to one second a day, a day is made up of 86400 seconds. The previous governments, both union and state, have pushed the construction of latrines in various names. As bureaucrats and elected village leaders found multiple ways to show the numbers to the government and siphoned off the money.

![SBM Dashboard](/images/sbm_dashboard.PNG)

As of 14th, July 2019, while writing the review, [SBM website dashboard](https://sbm.gov.in/sbmdashboard/Default.aspx) displays 30 UT and states are open defecation free, out of 36 entities, 9.74 lakhs toilets constructed, 5.69 lakh villages and 622 districts out of 725 districts are open defecations free since 2014. What an official lie! Other than Goa and Odisha all other entities in India, 90% of household possess a toilet. States like Bihar, Harayana, Uttar Pradesh has 100% open defecation free. Such are government records.


### What can we do about it?


Dr.B.R Ambedkar said, "the constitution India will make untouchability illegal, but it may take more than 100 years or so for society to stop practicing it". The caste and untouchability always comes in different forms and sometimes in disguised as rich and poor, urban and rural, clean and unclean, etc. Unless people are aware of all the consequences and society decides  to move forward, as authors note it may take five more decades to eliminate open defecation with a rate of 1% decline in every year. The survey carried out by authors reinforces caste biases and superiority feelings among the various castes: "16% of respondents identified themselves as forward castes to the surveyors, at least one of the family member practice untouchability". The society has a whole needs to move towards not a few individuals. As you see, the benefactors of the caste system want to assert the power and privilege.


The data government collects incomplete and inaccessible to the majority. The government is only interested in counting several toilets rather than it's daily usages among the family members. As the authors note down three crucial points in the final chapter

- There is a need for a separate survey to collect open defecation data, the data accessible to all, and policymakers should come up with realistic targets for elimination.
- Social awareness is vital for humanity's progress, speak about choices, caste, and culture influence. Policymakers should be aware of these factors and make policies accordingly. 80% of the people own a mobile phone and defecate on the phone. These people, unaware of the consequences of open defecation and rightfully aware of the need for mobile phones. Without a healthy body and mind, wealth has no meaning.
- Test and modify strategies about pollution and pits; there are a lot of myths around the purity of having a toilet next to the house, holes may fill soon, cleaning them is difficult.


Open defecation is the biggest obstacle and shame on human civilization in the 21st century. This book has come out at a crucial time and speaks the most neglected topic among Indians.