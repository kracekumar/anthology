+++
date = "2017-07-28T1:35:21+05:30"
title = "Awry"
tags = ["musing"]
+++

Quite often thunderstorm hits at night. The monsoon night is dark, cold, and breezy. When downpour starts most folks inside the building peek outside. Stranded humans, animals hate the rain and look for shelter. It stops the routine. With improper drainage and umbrella, I think twice to get out. It’s ever classic green scene to hold a cup of tea on the left hand, hold a book in the right hand, stand in the verandah, enjoy the moment, and feel the moist air hitting the face.

I don't want to enjoy the rain, since it was already 9 PM, and want to get back to home. Today, I didn't cycle and didn't check weather app before leaving home. It was pouring outside and booked an Ola share. The vehicle was a minute away from the destination. From the first floor, I marched towards the ground in the staircase. As soon as I reached the ground floor, the phone in my right hand started ringing. It was a call from the driver and informed me; "I'm already in the pick up where are you?" I enquired in Kannada, "is your car white color, and have you switched on parking indicator." He replied, yes.

I ran towards the car, without an umbrella and handkerchief was in my back pant pocket. After reaching the left side back door of the car, I pulled the door latch with the left hand. There was a co-passenger seated in the front seat. I removed the bag from the shoulder and placed it on the right side of me in the back seat. The driver asked me "where is the drop point?" I said "Sarjapur Road." The driver and co-passenger heaved. The driver stated, "I'm planning to drop him in Koramangala, and head back home. Can you cancel the ride?"

Now it was my turn to take a deep breath, but I didn't. I replied to the driver, go ahead and cancel yourself. He answered, if I don't enter OTP in next two minutes, the system cancels the ride. I didn't know what to respond. I sat in the car for 10 seconds thinking should I bounce back, and force the driver to take the trip, then I got down the vehicle without thanking the driver. I ran back to the office entrance and stood under the roof, and wiped the phone screen with the t-shirt bottom part. Unlocked the phone, Ola app timer was moving toward the right end, with one minute to lapse. Out of frustration, I canceled the trip and booked an another share. After five minutes the cab showed up, I stepped into the cab, and reached home in fifteen minutes. When I got off the cab, the rain was in full swing and doing its job.

Rain brings joy to farmers, blossoms flower, cools the surface, lashes out dirt, spreads stinking odor in slums, and low lying areas, and mosquitos breeds in stagnant water. It was humiliating for me to get down the cab. Everyone enjoys the rain inside the house but hates when outdoor. What did the rain lash from me? Did it cool me down? Did it suppress the turbulent argument?
