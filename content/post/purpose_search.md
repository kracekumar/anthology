+++
date = "2018-10-20T00:50:21+05:30"
title = "Purpose Search"
tags = ["free-verse", "musing", "lonely"]
+++

The journey of the search, purpose,<br/>
paved the roads to the<br/>
silent, and serene resting place.<br/>

Tracing back to the origin<br/>
to follow the pointless years ahead<br/>
filled with disappointment, and destituteness.<br/>

The months marched in a row,<br/>
the weeks walked fast,<br/>
the days dazzling with sunshine,<br/>
the nights not leaving anything unturned,<br/>
the winter winds bringing all the unholdable thoughts,<br/>
and clearing the air to view the farthest star,<br/>
I can look at the star all night,<br/>
but can't see it. <br/>