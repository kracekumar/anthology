+++
date = "2018-11-24T23:00:21+05:30"
title = "What is a new year?"
tags = ["free-verse", "musing"]
+++

What is a new year?<br/>
<br/>
The continuation of time without any significance.<br/>
<br/>
What is a new year?<br/>
<br/>
A way to cheat oneself with ignorance,<br/>
The days will be better,<br/>
All the worries will disappear,<br/>
The new light will spread happiness.<br/>
<br/>
<br/>
What is a new year?<br/>
<br/>
Logic and reason less way to<br/>
Pin faith on the future foolishly.<br/>
<br/>
What is a new year?<br/>
<br/>
Continue the same old finite recurring thoughts throughout the year.<br/>
A dream that’s oversold.<br/>



