+++
date = "2018-10-06T04:50:21+05:30"
title = "Reflected"
tags = ["free-verse"]
+++

The moon has no light on its own, <br/>
it reflects the sun’s light. <br/>
When the light falls on the earth <br/>
it’s moon’s light, <br/>
is it my goodness <br/>
all from you? <br/>

