+++
date = "2018-12-09T23:37:21+05:30"
title = "A tall stout tree"
tags = ["free-verse", "musing"]
+++

A stout, straight tree,<br/>
tallest in the park,<br/>
no grass surrounding the tree,<br/>
dogs urinate on it,<br/>
children play ‘touch me’ game around the tree,<br/>
lovers rest on the trunk and talk all day,<br/>
families sit in a circle and eat food,<br/>
crows feed on the litter,<br/>
photographer clicks a cute standing next to the tree,<br/>
bibliophile lays beneath the tree reading ‘White nights,’<br/>
long ago a bird dropped the seed,<br/>
the seed started growing in the night.<br/>