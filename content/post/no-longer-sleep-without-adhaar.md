+++
date = "2017-08-05T1:35:21+05:30"
title = "No longer sleep without Aadhaar"
tags = ["translation", "free verse"]
+++

The verse is an English translation of Tamil Poem penned by Manushyaputhiran published in [Charu Nivedita's blog](http://charuonline.com/blog/?p=6093).

Without Aadhaar Number

I cannot obtain the death certificate

they said


I sent out the news to all

I will depart today

Exactly in the evening

At six o clock.

At the last moment, they interfered

Pestered for a number


To die

I have a bag full of records

A caste certificate

A religion certificate

A ration card

An education certificate

Above all

A birth certificate

The chief should understand

I have born to die



I'm running out of time

I had promised to my friends

I'll leave today

I have to keep up my word

The chief

I can understand your concern

Even if I reach heaven or hell

I will be asked to show Aadhaar

Hence you're hurrying up

You have convinced me

Without leaving this place

If I circle here as a spirit

I need Aadhaar


I'm running out of time

I had promised to my friends

I'll leave today

I have to keep up my word

The chief

I can understand your concern

Even if I reach heaven or hell

I will be asked to show Aadhaar

Hence you're hurrying up

You have convinced me

Without leaving this place

Even if I circle here as a spirit

I need Aadhaar


The chief approaches a human

when they depart

opens the eyelid

And captures a copy

Then you click at the corpse

Then you affix the photo

Carrying the Aadhaar of the body

In the hand

Why do you behave like stealing from the dead?

Or why can't you leave the habit of

Stealing from the dead?



The chief

Allow me to die

Without Aadhar

After my death,

I have a secret plan

Born as kitten

I will look for my lover

Born as an eagle

I wish to circle above your head

For the God's injustice to me

I will find out their house and revenge them

You enroll me in Aadhaar

To monitor

All my activities post death.

That's your plan


The chief

Death is privy

Death is freeing from power

Death is boycotting punishments

Death is beyond laws

Death doesn't follow rules

Death means returning to origin

Death means residing in an emperor less island unattached


The chief

Yesterday you made me

Queue up in front of the bank

Now it's Mortuary entrance


Show me mercy

I have only half an hour in hand

- Manushyaputhiran
