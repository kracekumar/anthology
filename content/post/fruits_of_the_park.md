+++
date = "2018-11-29T00:22:21+05:30"
title = "Fruits of the park"
tags = ["free-verse", "musing"]
+++

<br/>
The unnamed bird's chirp<br/>
The squirrels run down from the tree<br/>
The puppies play in the grass<br/>
The public circle inside the park<br/>
The families relax beneath the shades of the tree<br/>
The kids' cycle around the park<br/>
A blind man walks alone.<br/>

