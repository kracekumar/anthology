+++
date = "2018-03-30T18:35:21+05:30"
title = "trouble"
tags = ["free-verse"]
+++

Each day arrives without any delay <br/>
Like a page after a page in a novel<br/>
Each day walks in with a list of troubles<br/>
Like a page filled with cryptic messages<br/>

In the middle of all the troubles<br/>
Where to find the peace, serene, and joy?<br/>

In a tasteful food?<br/>
In a warm green tea?<br/>
In the night breeze kissing the face?<br/>
In a returned gaze from the other side of the road?<br/>

In a child’s broken conversation?<br/>
In a well written sentence?<br/>
In a thoughtful remark?<br/>
In an eye pleasing portrait?<br/>

In a melancholic song?<br/>
In a glee on a couple’s face?<br/>
In a sensitive poem?<br/>
In a nostalgic photo?<br/>
In a dog’s lick?<br/>
In the bed alone?<br/>
