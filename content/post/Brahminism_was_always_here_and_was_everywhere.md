+++
date = "2020-01-07T02:25:21+05:30"
title = "Brahminism was always here and was everywhere"
tags = ["Society", "Brahminism", "Caste"]
+++
Now Zoho's CEO, Sridhar Vembu decision to participate in [RSS program](https://pbs.twimg.com/media/ENk1TPrVAAEV1oL?format=jpg&name=medium), 
"Resurgent Bharath" and a former employee, Saravanan Raja, 
resignation [blog post](https://medium.com/@saravanaraja_17675/dear-kannan-gopinathan-i-quit-zoho-for-the-same-reason-where-do-we-go-from-here-6ace2dba7a2a) 
published in August 2019, is creating decision in Twitter. When I first read it, it was *not surprising* to me. 

### Personal Experience

Last nine years, I lived in Bangalore. And worked as a software engineer in both the product and service industry with Indian, 
Indian-origin and Foreign bosses. Co-workers from various states of India, some went to IITs, other prestigious institutions, 
and some didn't mention at all. 

A quite significant number of people believe and exhibit Hinduvata views, 
the real name Brahminism - *"Negation of the spirit of Liberty, Equality, and Fraternity"* by Dr. BR Ambedkar.
Don't be taken away by the media or Wikipedia page description of Hinduvata and Brahminism.

Whatever India is witnessing today, NRC - "Your papers, please" was used in [Nazi Regime](https://en.wikipedia.org/wiki/Your_papers,_please). 
The planting of the attack, blaming for the terror, using religion as a reason to divide was in the Nazi playbook. 
Whatever we see today, may resemble shadows of Nazi regime but Indian version is deeply rooted in Brahminism. 
I align with Dr. BR Ambedkar's views in http://www.columbia.edu/itc/mealac/pritchett/00ambedkar/ambedkar_partition/513.html#part_6 
and hence call it Brahminism.

> If [the] Hindu Raj does become a fact, it will, no doubt, be the greatest calamity for this country. No matter what the Hindus say, Hinduism is a menace to liberty, equality and fraternity. On that account it is incompatible with democracy. Hindu Raj must be prevented at any cost.

### Counting Votes

Stating the obvious, each one of us is part of society. 
The IT crowd is no exception, a drawn sample from society. 
The behavior of the sample reflects society. I couldn't come up with the walk of professions 
which can be collectively called progressive - writers, artists, engineers, doctors, auto-drivers, legal systems, etc. 
No categorized private or public sectors are against Brahminism.

Whatever is happening in the country, is expected from BJP. 
In 2019, general election, [BJP's vote share was 37.4%](https://en.wikipedia.org/wiki/2019_Indian_general_election) across India. 
BJP failed to secure a seat in TN, Andhra, and Kerala. 
After removing Muslim voters from the total percentage of the voters, 
it may be said, at least half of the voters accepted BJP for their ideology or anti-congress or whatever reason. 
Now tell me why I should be surprised if a top executive at a successful company is attending an event organized by RSS. 
There needs no explanation about the connection between RSS and BJP.

### Benefactors of Brahminism

The main benefactors of Brahminism are upper castes. 
There is a higher dominance of upper castes in non-reserved government jobs and private sectors. 

In the book, "An Uncertain Glory", under the chapter, *"the grip of inequality"*, from the citation 18, 
*out of top 1000 Indian companies by assets, more than 90% of their members were upper-castes and 45% were Brahmins, Baniyas accounted 46%, 
SC/ST merely 3.5%.* 

In the 1990s, Khuswant Singh wrote a piece, [The Almighty Brahmin!](https://kalyankrishna4886.files.wordpress.com/2013/09/the-almighty-brahmin.pdf) 
he says the following

> My statistics come from a pen friend, Brother Stanny, of St. Anne's Church of Dhule in Maharashtra. He has compiled figures of different castes in government employment during British rule, the largest proportion of government jobs (40%) were held by Kayasthas. Today* their figure has dropped to 7%. Next came the Muslims who were given special provileges by the British. They had 35% jobs in 1953, in free India their representation has dropped to 3.5%. Christians, likewise favoured by the English, had 15%, their figure has dropped to 1%. Scheduled Castes, tribes and backward classes, who had hardly any government jobs, have achieved a representation of 9%, have achieved a representation of 9%. But the most striking contrast is in the employment of Brahmins. Under the British they had 3% - fractionally less than the proportion of their population; today they hold as much as 70% of government jobs. I presume the figure refers only to gazetted posts. In the senior echelons of the civil service from the rank of deputy secretaries upward, out of 500 there are 310 Brahmins, i.e. 63%. Of the 26 state chief secretaties, 19 are Brahmins; of the 27 Governors and Lt. Governors 13 are Brahmins; of the 16 Supreme Court Judges, 9 are Brahmins; of the 330 judges of High Courts, 166 are Brahmins; of 140 ambassadors, 58 are Brahmins; of the total 3,300 IAS officers, 76 are Brahmins. They do equally well in electoral posts; of the 508 Lok Sabha members, 190 were Brahmins; of 244 in the Rajya Sabha, 89 are Brahmins. These statistics clearly prove that this 3.5% of Brahmin community of India holds between 36% to 63% of all the plum jobs available in the country. How this has come about I do not know. But I can scarcely believe that it is entirely due to the Brahmin's higher IQ.

As of 2019, Brahmin born employees' numbers in the various mentioned field may vary by how much I'm unaware, but the trend remains the same. 
[Example, 2019 election results in the cow-belt](https://indianexpress.com/article/explained/in-hindi-heartland-upper-castes-dominate-new-house-5747511/)

Clearly, there is a pattern; being born in upper castes/Baniya background irrespectively helps one to reach top positions. 
Because of the various privileges - access to knowledge, contacts, resources, social structure, etc.…  

In India, 10% of the people control 70% of the country's wealth(forgot the source). It would be interesting to note which varnas this 10% belong to.

At the same time, in electoral politics, depending on the state, certain BC/OBC castes may have an edge, but that's for another day.

I'm not concluding all of the upper castes, baniya background, and dominant castes support RSS or Brahminism. 
There were/are Brahmins against Brahminism and other caste folks against Brahminism. They are minuscule in numbers. 
The majority of foot soldiers of RSS may be Shudras and Dalits men. Still, brains behind RSS and Sangh Parivar are always upper and dominant castes men.

### Tough fight

It's easy to spot and fight the folks who support Brahminism. 

- What about the ones who don't declare their stance?
- What about the ones who have mastered the language, live a layered life, and hard to identify?
- What about the folks who claim to be progressive, use the proxy and distracting term for Brahminism?
- What about the folks who see only class and refuse to perceive caste?

### My privilege

While writing, I'm aware of my privilege of being a male in the dominant male society. English speaking men in the country. 
I don't claim to be a perfect man, even today or yesterday.
I have made mistakes in the past and had conservative views then.

### Final thoughts

Saravanan Raja quit his job in Zoho. I appreciate the stand. 
This is not the very first time I have heard or read similar stories. 
The story of the excellent math professor, [Vasanta Kandasamy](http://vasantha.in/Biodata.htm),
who was denied equality, equal opportunity, promotions, 
and had fought her 28 years of her life against Brahminism and Brahmin in IIT Chennai. 
You can hear the complete struggle of her being born as Women and Dalit, 
in the [Tamil interview](https://www.youtube.com/watch?v=8MvIlsk6Cgk&feature=youtu.be).

You can quit one job, two job, or as many jobs you can, 
boycott their products, but can we escape from the society which is entrenched in Brahminism?
Can we escape from co-workers, your professional community, which is just a subset practicing 
varying degrees of Brahminism? We don't live in the times of Buddha to turn ourselves 
into Bhikku or wander for food in the woods.
