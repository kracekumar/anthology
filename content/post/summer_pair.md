+++
date = "2018-04-06T21:35:21+05:30"
title = "summer pair"
tags = ["free-verse"]
+++

In the winter nights <br/>
We sat in the balcony <br/>
Looking at each other <br/>
Eyes out of fear <br/>

Everyday we saw each other <br/>
And enjoyed the night silence <br/>
In the calm breeze <br/>
The lonely feel <br/>

Once summer arrived <br/>
You departed <br/>
Everyday in the night <br/>
I stood in the balcony <br/>
Gazing the sky <br/>

After a full moon day <br/>
I filled the water bowls in the balcony <br/>
You arrived with the partner <br/>
And stood on the railing in the afternoon <br/>
