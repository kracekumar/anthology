+++
date = "2018-03-25T13:22:21+05:30"
title = "wander"
tags = ["verse"]
+++

I wander in the nights<br/>
In search of missing gem<br/>
Neither I know what it is<br/>
Nor where it is<br/>

Each step weighs heavy in the night<br/>
A balcony bulb sheds light in the apartment<br/>
Roads gushing with vehicles<br/>
Bored dogs howling<br/>

The summer night breeze missing<br/>
Cigarette smokes marching out of the tea shop<br/>
Apartment Security guard in the cabinet with half closed eyes<br/>
Day laborers returning home with equipment<br/>

I crawled over the neighborhood<br/>
Like a street light on the deserted alley<br/>
Until my heels hurt<br/>
To tire me for the day<br/>

