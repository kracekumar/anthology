+++
date = "2020-04-17T20:25:21+05:30"
title = "A Few Questions to the Devotees"
tags = ["Society", "Periyar", "Religion", "Translation"]
+++

This is the English translation of Periyar's work published under the title, "பக்தர்களுக்கு சில கேள்விகள்"

The work is available under Copy Left license and translated PDFs link in English, Tamil and English available at the bottom of the page.
Please feel free to share and use it. For suggestions and comments, you can reach to me in [Twitter](https://twitter.com/kracetheking).


### A few questions to the devotees

1. If God created the universe, who created God?

2. When there is no roof for moving humans to rest, is the temple a need for the immovable stone statue?

3. If childbirth is God's act, widows and sex workers delivering are whose act? Why?

4. When God's army exists, why does the military guard the border?

5. Why does the mighty almighty need lock and guard?

6. If everything is God's act, then whose actions are cyclones and floods?

7. If in God's creation, all are equal, why is there a divide, capitalist and laborers, parpaans and pariyaans?

8. Why do ten times descended God and don't descend once to control commodity price?

9. If no atom moves without God, then because of whose act does temple statues travel abroad?

10. Why does the embodiment of love, God need lethal weapons?

11. If childbirth is God's power, can God create birth after family planning?

12. Is God in an atom, absent in the atom bomb?

13.  You offer food to God, doesn't it excrete?

14. Is the God He? She? They? Or It?

15. If destiny drives everything, why do you need God?

16. Show me a devotee who asked boon for the public good?

17. If God is powerful, why didn't God imbibe good thoughts in human knowledge?

18. Why do illiterates exist in the birthplace of Saraswathi, Goddess of knowledge?

19. Why do poverty and ignorance overflow in the country full of devotes?

20. Why are there no food and jobs for three crores people in India, even when there are thirty crore gods?

21. God, who created theists, why did God create atheists?

22. When there is no milk for the crying baby, why anoint milk in a stone statue?

23. Is it fair to consume God who descended as pig and fish?

24. When atheists burn God's image, the devotees are angry. Can devotees burn the crackers with God's image?

25. The devotees who offer hair to God, why not donate hands or legs?

26. The devotees who claim, singing Thirupugal smells fragrance, will they stop brushing?

27. When the baby is born similar to Pillayar, will you prattle or fear?

28. As devotees say, diseases are God's punishment, then why do they rush to doctor as soon as they are diseased?

29. What is God's purpose in Tamil Nadu, who doesn't understand Tamil?

30. Do you need an eighty kilograms chariot for eight kilograms of God's statue?

31. Who prevents smallpox, Goddess Mari Amman, or hospital?

32. Who is responsible for education? Saraswathy or Teacher?

33. Can a devotee pass the examination without studying?

34. The devotees in the morning sight(darshan) brahminy kite(Garudan) in the sky.
When a brahimy kite picks up the chicken, why do devotees stone?

35. Today, If Gokula Kannan is born, are gopis ready?

36. Ayyappan devotees follow ascetic life for forty days. How do they behave for the rest of the year?

37. My dear devotees, who visit Ayyappan in Kerala, what shall we do Tamil Nadu gods?

38. Devotees, who worship fire, why do you scream when your house is on fire?

39. Are parpaans, pastors, and mullahs messengers of heaven?

40. Do space travel countries celebrate Ayutha Pooja(Vijayadhasami)?

41. What are the conditions prohibiting God to live in Russia and China?

42. Sai Baba, with mantras, brings a Nuptial thread(Thalli, not food), can he bring a live elephant in front?

43. The priest who drinks raw blood, can he drink the poison?

44. The devotees who claim Nama Shivaya cures danger, will they touch electricity?

45. If firewalking is God's power, will devotees demonstrate rolling in the fire?

46. If carrying a fire-pot is because of divine power, are devotees ready to dip in the boiling oil?

47. The devotees offer sacrificial goat, chicken, and domestic animals. Why don't they offer a tiger, lion, and other wild animals? Are they afraid?

48. Did any paarpaan ever perform a kavadi dance?

49. Has any paarpaan performed ghost dance or spirit dance?

50. Why do married couples fight, suffer from poverty, and die even after precise horoscope matching?

51. Why do deeds fail even after looking for an auspicious day, time, horoscope, and omens?

52. Why do you need a police station when astrologers and magicians exist?

53. Can an astrologer spot the black money residences?

54. Are rahu kaalam, omen followed only in our India?

55. Other than India, does astrology exist in any socialist country?

56. Why didn't astrologers foretell the killing of Gandhi, Kennedy, Indira, and Rajiv?

57. When the world accepts men's remarriage, why do they curse women's remarriage?

58. If the name of the woman after their spouse's death is the widow, what is the name of the man after their spouse's death?

59. Nuptial thread(Thalli) is the symbol of women's wedding, what's the symbol of the men's wedding?

60. When men add a caste surname, why didn't women add a caste surname?

61. When rebirth exists, who do you perform thithi to?

62. On the concept of rebirth, if lives are born, why do the population explode?

63. Who is the founder of Hinduism? What is the evidence for it?

64. Is Dhraupthi, a chaste wife with five husbands?

65. The devotees who panic when I beat Ram, are you fine trampling Bhooma Devi in slippers every day?

66. Do wise people agree, Dhasaradhan married sixty thousand wives?

67.  Devas and saints arranged Raman - Sita wedding on auspicious time. Should it lead to exile?

68. Was it an ethical act for Hanuman to light Lanka in the fire?

69. Raman disgraced Ravanan's sister. How is he a moral God?

70. According to the Purana, when Ravanan touches a girl without her consent, his head will explode into pieces. Why did the curse fail in Sita's case?

71. Raman, the archer, killed Vaali hiding, is it bravery?

72. Is it fair for Ram to kill Dravidian Shambhukan for offering tapas?

73. Ram and Laxman tried to commit suicide in the Sarayu river. Is it the nature of God?

74. Out of Valmiki Ramayanam, Kambar Ramayanam, Tulsi Ramayanam, which one is the authentic Raman's story?

75. After sixty-three Nayanars(teachers of Shiva), twelve Alwars(teachers of Vishnu), Kannan, proclaimed he created the four-fold caste system(varna) in Bhagavad Gita. No disgrace?

76. When your lord Muruga marries out of the caste, Why do you hate when a Hindu engages in intercaste marriage?

77. Till Krishna was delivering Geetha counsel in war, where were Gaurvas? Were they plucking flowers?

78. In atheist China, Russia, there are no Saraswatis and sex-workers! Why?

79. (In the Bible), Jesus created light, then who created darkness?

80. As per the Bible, the earth is flat. How many Christians accept the statement?

81. When pastors say prayers can heal the disease, why do Christian hospitals employ doctors?

82. Who created the devil(satan)? Why did they create the devil? Why should we oppose the devil?

83. When a damsel delivers a baby without a husband, will you patiently call it as a godly act(holy ghost)?

84. Do doctors accept Jesus's birth?

85. Where is blood and flesh in Eucharist? Think?

86. When devas exist, why do we need pastors to forgive sins?

87. If fifty crore Hindus are purified, can they attain salvation? Previously, how many priests attained salvation?

88. Why do relatives of the deceased cry, when the deceased wishes to reach heaven, Vishnu's heaven, Shiva's heaven and attain salvation?

89. What are the outcomes of the six-time prayers in Hindu temples, five times prayers of Muslims, the whole night prayer of Christians? What else, other than wasting time and material?

90. If holy ash removes harmful fluids and germ, why do you smear holy ash in an iron safe and weight scale? Did harmful fluids and germs invade these? Then why haven't you applied holy ash in the organs secreting harmful fluids?

91. Even when Swami Sankaracharya, Swami Kirubananda Variyar smear holy ash, why do they suffer from the disease?

92. Can a woman long live with turmeric and traditional vermilion? Do happy women all over the world roam with turmeric and vermillion in their forehead?

93. You tell the bride to place her right leg first while entering the bridegroom's house. But the country's army men move their left leg as the first move. In the two legs, is there a good and bad leg?

94. If the planetary positions determine the life of a baby, then the lives of all the babies born during the same time should be similar. Does this happen? Did anyone track and test?

95. A baby dies after birth. Then all the babies born in the time should also die. Does it happen?

96. Only if there is clarity on the timing of the birth astrology can predict, then birth time becomes crucial. What is birth time?
    - Is it the time when a fetus formed?
    - Is it the time when a baby's head first protruded?
    - Is it the time when a child's whole body appeared?
    - Is it the time when the umbilical cord cut?
    - Is it the operation time in Caesarean?
    - Is it the time when a child placed in an incubator in preterm birth?
    - Is it the time when a child lifted out of the incubator in preterm birth?
    - Is it time when the child separated from the mother?

97. If planetary positions determine one's life, what's the role of God, destiny, and past birth benefits?

98. When the time of birth and planetary positions determines life, then why do we pray to God? Why are there countless Shastra practices? Can prayers and practices change destiny?

99. If life is destined after birth, then, why do we look for an inauspicious time like Rahu kaalam, Ashtami, Navami, Yemma Kandam?

100. After believing in astrology's axiom, planetary positions govern life, what is the place for God in our lives?

101. When we enquire how the Hindu scholars, Veda Shastra authors, sages, and rishis were born. The answer is, they are born to Kalaikottur Rishi Deer, Kousik Kusam, Jambukar Jackal, Gouthamar Bull, Mandaviyar Frog, Kangaeyar Ass, Sounagar Dog, Ganathar Owl, Sugar Parrot, Jambuvantar Bear, Agasthiar Vessel, Aswathaman Horse.  Is it believable in the twentieth century? Is it possible to spread religious preaching without modification?

102. The current birth depends on past birth by God. Then why didn't God create all of the beings good in their first birth! Why does God create one to do good and another to do evil?

103. After creating an evil man in the current birth and punishing him in his rebirth, can we call such a creator God?

104. Hundreds of soldiers die at the same time; does lifeline(palmistry) show their death at the exact time?



<b> Source </b>: http://dvkperiyar.com/wp-content/uploads/2015/08/42.pdf

<b> Notes </b>:

1. Paarpaan - Periyar used the word to denote Brahmins. <br/>

2. Pillayar - Another name for Ganesha or Vinayaka in Tamil Nadu. <br/>

3. Mariamman - A Tamil folk goddess focussed on rains and curing diseases like cholera, smallpox, and chicken pox. <br/>

4. Kavadi (burden) - is a ceremonial sacrifice and offering practiced by devotees during the worship of Lord Murugan. <br/>

5. Spirit Dance - A form of worship when a spirit of the god enters the person and the person dances. <br/>

[Download English Version](/uploads/A_few_questions_to_devotees_en_v4.pdf)<br/>
[Download bilingual Version - Tamil and English](/uploads/A_few_questions_to_devotees_ta_and_en_v4.pdf)<br/>

<b> Translated by: R. Kracekumar </b>

<a rel="license" href="http://creativecommons.org/licenses/by/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by/4.0/88x31.png" /></a><br />This work is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by/4.0/">Creative Commons Attribution 4.0 International License</a>.
