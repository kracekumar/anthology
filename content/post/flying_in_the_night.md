+++
date = "2018-11-24T23:32:21+05:30"
title = "Flying in the night"
tags = ["free-verse", "musing"]
+++
<br/>
I fly in the night<br/>
Like the bird questing for the nest.<br/>
<br/>
I see the dispersed clouds in the blue sky<br/>
Like the freedom to roam.<br/>
<br/>
I scrap the papers in the street<br/>
Like cleaning the heart.<br/>
<br/>
I fell asleep and woke up<br/>
Like a security guard in the gate.<br/>
<br/>
I feel I found a purpose of life<br/>
Like the howling street dog in the night.<br/>
<br/>
I thought I lived life <br/>
Like a forgotten lighthouse in the harbor.<br/>