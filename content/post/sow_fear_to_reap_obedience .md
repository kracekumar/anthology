+++
date = "2020-03-27T00:02:13+05:30"
title = "Sow Fear to Reap Obedience"
tags = ["Society", "Police"]
+++

The fear is a weapon targeted at an individual, group to exert power to control, reap benefits for the imposer, and maintain the status quo. Religion has understood it very well today and has used it against the followers and no need to say who are the benefactors.

Videos are circulating in social media, where police stop migrant workers and beat them, hit the vegetable carts, stand outside the mosque and beat the men, etc. I'm not defending persons who step out for non-essential tasks. 
<a href="https://tamil.news18.com/news/tamil-nadu/youth-argued-with-police-show-coronavirus-in-aranthangi-vaiju-272351.html">
There was a case in Tamil Nadu, where one guy, questions the police to show him the coronavirus, then he would stop coming out of his house(<strike>Dang, I couldn't find it now</strike>).</a> The edited version of the video is funny for his cinematic style dialogues and more reasons. Whatsoever, said, beating or violent methods are never a solution. If the lathi charge is the thesis, what is the anti-thesis?

As a society(illusion?), the mindset to sow fear and reap obedience is deep-rooted inside and constructed historically. In other words, the structure of society propagates fear through the hierarchy.

### School

During school days, the teachers beat us black blue if we fail in exams or don't complete assignments. The scene was the same in government or private schools. Each teacher had their favorite canes and favorite spots to hit the students—some in hand, some in the leg.  Few teachers devised a pinching strategy with their thick nails, in hand, in the belly. These punishments would differ for girls and boys. The parents gave no amount of mercy to their children about it. 

The idea behind the beating the students always followed simple line - fear will make you complete homework to follow my instruction, which is good for you. Even at a young age, it was natural for most of us to know how to take advantage of the situation and the person. Some students would do the home works of the strict teacher and take leeway for the lenient teacher. Few students never bothered to complete any homework and happy to receive the beatings. They would make all sorts of the sounds while receiving the blow, once they reach the desks, they smile at others, say, "this is nothing at all," "Haha, I'm numb to all the sticks." 

I never saw any teacher beating the children of school headmistress or headmaster or correspondent or politician's children when they got low grades and didn't complete homework or assignments.

My ears didn't hear the stories where teachers spoke among themselves; why don't students do homework or score low marks. It's always you're stupid(Makku in Tamil).

### Thief

In the early 2000s, in our neighbor's house, a thief stole a gold chain from the windowed lady. He jumped from the first floor, landed in a sandpit, and raced to escape. His bad luck, the slippers tore, and he fell into the ditch, 500 meters away from the house. In the next ten minutes, the shouting was aloud; all the men started chasing for the guy; the four guys who were returning from the late shift in the nearby mill caught him and dragged him to the temple entrance and tied him in the tree. 

The lady got back her chain and cash. A group of men split the extra cash in his pocket. For one hour, he never opened the mouth; few men sexually abused his mother and wife—no response from him. By now, the crowd has gathered in huge numbers and looking at the man. They signaled all the women to leave the place. A group of boys and men stayed in the area.

Once all the women left the place, they removed his shirt, vetti, and made him stand with underwear and started beating with sticks and kicked in his stomach, knees all over his dark-skinned body. They didn't spare any skin in the body. By morning, his body was full of bruises, red spots and answered, where he was residing. They enquired and found out he even lied. After that, few more people took turns to beat him. 

Finally, someone asked about his caste, and then a group of men decided to hand over him to the police. The lady never went to the police station to complain about the night incident. What stopped these men from calling the police at midnight?

The incidents like these never end - a six-year-old boy beaten up for plucking a mango from the tree, the domestic violence of husband, instigating violence to vandalize colonies, imposing how Dalits should dress, and so on. 

Today's mob lynching is an extension of the same mindset, who controls whom, who exerts moral and authoritative power, and justice is defined by the caste and not by what is the crime.

### Anniyan Movie

Corruption is always the talk in the media.  During office lunch, elections, and the conversation goes on and along with the crime. You may have watched the Tamil movie [Anniyan](https://en.wikipedia.org/wiki/Anniyan). Read it the Wikipedia, if you have not watched it. 

The protoganist kills the corrupt individuals in the most gruesome manner. The detective/police try to find out the killer and where does the killer get the idea—nothing new in the plot. What's interesting is the use of peace-loving Hindu text, [Garuda Puranam](https://en.wikipedia.org/wiki/Garuda_Purana) to devise the punishments for wrongdoers. Even in Shankar's earlier movie, Indian, he used the same concept of instilling fear as a way to correct society. Instant punishment, the killing of the corrupt person in a gruesome way, instills fear in the community. As a result, everyone follows the orders and to live in harmony. Did we enquire why crimes happen? How to avoid it? Never. Religion is all about answers and never nudges you to ask questions. 

All the stories of the so-called Hindu God's avatar have associated the violent killing of demons or asuras. The psychological use of the idea drives us to use violence in a lot of cases like parents instructing kids not to do an activity by creating fear of Ghost, Bogeyman, disappearance of the most vocal activist in the society, humiliating the progressive thinkers, etc.

Can we separate the texts from its followers and supporters of the suggested methodology? Who will psychoanalysis the authors and the supporters? 

There are a lot of individuals, ask the questions, 
- Who am I? 
- What's the meaning of life?
- What's the purpose of life?.

But rarely, the individuals, ask the questions
- What's the meaning of a society?
- What's the purpose of society?

Unlike life, which has no meaning, society is a construct. Humans make society, and sometimes it's even powerful than the governments. No one can escape it.

You can't understand the mindset of the police by isolating the case of breaking the order during the pandemic. Though the concept of lathi charge came into existence during colonial time, the idea of violence, instilling fear, has been in the society and religion for a long time. The same approach makes the citizens support the death penalty, encounters; the same approach allows police to shoot the protestors.