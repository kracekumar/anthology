+++
date = "2019-07-06T20:20:21+05:30"
title = "What doesn't pain?"
tags = ["free-verse"]
+++

What doesn’t pain in life?<br/>
From very existence to every activity,<br/>
Love inflicts unfathomable mental burden,<br/>
shows all places which are filled with feelings,<br/>
which you never want to know.<br/>
<br/>

Work produces dissatisfaction of time<br/>
like ants laying the chemical path.<br/>
<br/>

Society breeds intolerance and disillusion<br/>
Art becomes a fashionable entertainment<br/>
Intoxicants are the only land of the refuge.<br/>
<br/>

Philosophy proves the life is walking around the globe<br/>
Friendships rarely lasts long,<br/>
always short, confined, consumed by timezone difference<br/>
two individuals with single mind.<br/>
<br/>

Death hides in the remote corner<br/>
where you never have control.<br/>
<br/>

Finally loneliness and dark sadness<br/>
grows as days pass<br/>
you wake up as<br/>
the victim of misery<br/>
like the law of nature<br/>
where you’re locked<br/>
inside the earth with no exit.<br/>
<br/>
