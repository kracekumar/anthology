+++
date = "2019-05-18T23:00:00+05:30"
title = "A Glimpse of Buddha's Teaching"
tags = ["Buddha", "Ambedkar", "Buddhism"]
+++

- Today is Gautama Buddha's Birth Anniversary(18th, May 2019). Buddha was a human who was born to Maya and Suddodhana 2600 years ago in a warrior clan. He is not a God, he was a human. He was forced to leave the empire because he refused to wage war with the neighboring empire.
- After wandering six years, he started delivering sermons everywhere, practiced his preaching. His teaching was grounded in rationalism, truths with evidence, removal of human suffering, never believed anything is infallible.
- He rejected the concept of infallibility of Vedas, God, Soul, Varna System, Caste, Animal sacrifices, Rituals, Ceremonies, Sins of previous birth, life after death, the idea of the rebirth of body and soul.
- He accepted mind is the leader of all faculties. To him, religion should not worry about heaven or hell, or whether God created the world or not, the essence of religion should be cleaning of the mind.
- Religion should speak about developing relationships with fellow human and sentient beings. Religion doesn't lie in the holy texts, whereas lies in the observances of the tenants.
- Buddha's Dhamma or doctrine is about the relationship of human in the earth, humans are living in sorrow, misery, and poverty, acknowledging the existence of suffering, and show the way of removing the suffering.
- How to remove the suffering is what eightfold path is about: Right view, Right Thinking, Right Speech, Right Action, Right Livelihood, Right Diligence, Right mindfulness, Right concentration.
- Buddha is revolutionary and democrat. There were no discriminations during his time among the followers, the followers were from all castes, all genders, all jobs, somewhere previously culprits, robbers, criminals too.
- Buddha didn't promise salvation to any followers, he said he was a Marga Data - way finder and not Moksha Data - a giver of salvation. His doctrine didn't have a place for himself, and it was not a revelation but only about the human relationships on earth.
- His teachings say what to do, what to avoid like don't get attached to anything, practice detachment, don't believe in supernatural, don't believe in the soul, don't believe in sacrifice, be compassionate and love all the sentient beings, birth is not the measure of human's worth, etc.
- A follower may be mendicant(Bhikkhu or Bhikkhuni) or layperson(Upasaka). One needn't follow the teaching of Buddha in the strictest form by becoming mendicant. Anyone can become mendicant become part of the Sangh.
- The most crucial part of being a mendicant is giving up property ownership, not following Dhamma, makes one expelled from Sangh or Fraternity. Different sects of Buddhism have different interpretations of his teachings.
- After 400 years of Buddha's teachings, the practitioners started writing down his teachings. Some sects call Gautama the last Buddha,  practice, believe in rebirth, Karma, and other ideas which were against his own teachings.
- The Buddha refused to appoint his successor, his view was 'the principle must live by itself, and no authority of any human. If the principle needs the authority of a human, then it's not principle'. His principle and religion were quite apart.
- Most of my understanding of Buddha's teachings come from two books: [Dr. B.R Ambedkar's, the Buddha and his Dhamma, critical edition by Aakash and Ajay](https://www.goodreads.com/book/show/10785310-the-buddha-and-his-dhamma), [The Heart of Buddha's teaching by Thich Nhat Hanh](https://www.goodreads.com/book/show/209574.The_Heart_of_the_Buddha_s_Teaching?ac=1&from_search=true).
