+++
date = "2018-11-24T23:10:21+05:30"
title = "The shadow of the tired"
tags = ["free-verse", "musing"]
+++

In slow-moving traffic,<br/>
A Fast beating heart,<br/>
Unsettling eyes,<br/>
Starting at the screen,<br/>
Devoid of curiosity,<br/>
Scared of old feeling rising,<br/>
All vehicles are still on the road,<br/>
Questioning the decision to travel,<br/>
There falls a shadow of wasted life.<br/>