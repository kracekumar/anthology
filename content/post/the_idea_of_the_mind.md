+++
date = "2018-11-24T22:56:21+05:30"
title = "The idea of the mind"
tags = ["free-verse", "musing"]
+++


Can the eyes resist seeing the new land?<br/>
Can the legs withdraw from the path?<br/>
Oh! New land,<br/>
What is there in the store this time?<br/>
Are you beautiful, <br/>
And soothing like rain?<br/>
Oh no, It’s an idea in the mind of the dead.<br/>
