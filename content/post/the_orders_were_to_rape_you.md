+++
date = "2021-03-30T00:25:21+05:30"
title = "the orders were to rape you - Book Review"
tags = ["Women Tigresses", "Eelam", "LTTE", "Meena Kandasamy"]
+++

![Book Cover from Navayana](https://navayana.org/wp-content/uploads/2021/02/The-Orders%E2%80%949788194865445%E2%80%94front-cover%E2%80%94low-res.jpg?v=c86ee0d9d7ed)


```
We will build the tomb
For women's exploitation
We will dig the graves
For society's backward ideas

- Captain Vaanathi
```

The warfare techniques evolved all along with technological advancements.
The combat weapons became - sword, gun powder, tanks, missiles, submarines, aircraft, automated missiles, cyber-attacks, biological weapons.
One everlasting weapon over centuries is rape. **Rape is an undocumented and loaded weapon in the military playbook and men's minds.**

LTTE and Tamil Eelam's freedom struggle is always a topic of discussion in Tamil Nadu since 1970s. As a kid, I listened to Sun news, especially the international news section, to know what's happening with LTTE and the Sri-Lankan government. All I wanted to hear - LTTE is making progress, they captured the base, they are marching forward, and they are winning the war! The students discussed it in lunch, the folks traveling in city/town buses discussed it. There were a lot of folks who were against LTTE too.

In the first part of the book, `the orders were to rape you`, Meena Kandasamy pens events that led to her long-term relationship for liberation, love (internal strength of her) Tamil-tigresses' and her support for self-determination of Tamils. The island nation's events traveled so far and entered into the author's home and homes of Tamil Nadu as it unfolded.

History is written by winners and sometimes by the survivors. Not all survivors wish to speak or possess the language or skill to record history. The second part of the book documents the female Tamil tigresses and a wife of a Tamil tiger. The honest account of their meetings, situations, and post-trauma they live with. Along with their trauma, the author accounts her personal pain, troubles, and shattered images of the tigress's conditions. The rape was a post-war tool of the Sri-Lanka army. The author points out how white-skin authors are considered neutral, whereas someone who can understand pain, the language, and state oppression is considered one-sided.

Here is an excerpt from the survivor

```
I don't know if you will get angry at me,
but I have to say this: this Tamil society is useless.
Why do they not work towards the betterment of their own condition?
Instead, they are putting all their time and effort into slander.
Instead, they dial long-distance to Sri Lanka to tell this story.
All of this because I do not have a husband.
```

Tamil Tigresses are regarded highly, and they were lethal fighters in the war. Society's image of them with uniform and without uniform reveals the Tamil society's double standard. The women's status in society is brought back to the pre-LTTE days. The no-matter the achievement of women, she is treated with all regressiveness. If the survivor was an unmarried male will society treat him the same way? It reminds us the progressiveness is a long-term and continuous engagement. The dignity of the co-survivor and love/empathy is missing in the community. The Tamil Tigress finely puts in the book `A corpse in awake is looked upon with more respect. A corpse is superior to them, to a raped woman like me.`

``` bash
"I have never killed anybody, it is true, but it is because
I lacked the courage or the time,
not because I lacked the desire" - Eduardo Galeano

```

The person undergoing rehab needs at utmost confidence and trust of the doctor to reveal the horrible events. While narrating the rehab discourse, the author points out the fine critic of the rehab program - `.. domesticating them, teaching them to make cakes and rear chickens and do embroidery - tasks for which these women have no patience.`


```
Everything is now a dream
many of my friends
are now on the battleground.
A few of them, in graveyards.
Me alone, with a pen in hand, a poet.

- Aadhilatchmi
```

Tamil poetry has a long tradition of male and female poets from the Sangam Era. The tigress's poems occupy the book's final part and light the reality of the struggle and social revolution female fighters envisioned. The poems burn throughout the book and also rings alarm bells to the oppressions.

These poems are troubling and aggressive. Every poem by the fighter is a universal poem against oppression and shakes society's consciousness. These poems also serve as tools to dismantle the structure of oppression. Was it a deliberate choice to place the poems(written earlier compared to other events) at the end to show the vision and to serve as a hope for the future?

Certain poems are coupled with time and place. These poems transcend time and place to become the tool to smash oppression and ring the death bell to oppressors. The poets and poems repeatedly burn to show the path and the equitable future for everyone and against every social and political inequality.

The lives of survivors are beyond painful, living in a foreign land with dreams of homeland and no control over their dreams. The sorrow, betrayal, agony, grief, the pain runs throughout the book.
The entire book is disturbing right from the title. The work also reflects the Tamil society's failure to look at the women's achievements in terms of bravery, confidence, fighting for the cause, and fighting to bring revolution, justice, and liberty. Tamil society's measurement of women concentrated in so-called moral standards - archaic past propagandized moral value is the start and end of all talents, virtues, sacrifices, dreams, and achievements.

The book is 100 pages, and the length is where it falls short. In the end, the book leaves you with the unjust pain and bravery of the fighters. The sacrifice of the women-fighters is still to be told in a longer length.

In every work, the author Meena Kandasamy experiments with her narrative, style, form, delivery, the boundary of the medium and succeeds by a great distance. In the latest work, she further expresses her pain, suffering, risks involved in interviews, and discomfort while recording the truth and fighting for justice in all forms - legal, social, moral, and existential. It's rare in a book where an author can write an essay, compile poems, uncover the pains and struggles of the brave young tigresses and herself. The book is a not ordinary book on account of the pain, rather creative in its compilation, approach, and human, sensitive and devastatingly brutal in documenting the pain and praiseless fights for a just, equal and honorable future.
