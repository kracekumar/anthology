+++
date = "2018-08-09T01:24:21+05:30"
title = "Kalaignar"
tags = ["free-verse", "musing"]
+++
 

ஆயிரத்திற்கும் மேலான அரசியல் குறளொலித்த தமிழ்நாட்டில் <br/>
நின் கரத்த குறள், குறளடற்றவனின் உரத்த குறள். <br/>

பாதங்களுக்குத் தெரியாது பாதை யாரிட்டது என்று, <br/>
நீ ஈட்ட பாதை எங்களைப் பல்லத்திற்கு இட்டுச்செல்ல அல்ல, <br/>
படிகட்டியில் ஏற்றி எங்களை உலகத்தைக் காணச் செய்ய. <br/>

ஆயிரமாண்டுகளுக்கு முன் சோழர்கள் சமம்படித்திய நிலம், <br/>
இன்று தமிழகத்திற்குச் சோறு இடுகிறது. <br/>
நீ செழுமைப்படுத்திய மொழி இன்று தனித்தன்மையுடன் ஒளிர்கிறது. <br/>

கார்ல் மொர்ஸ் எழுதிய உலகை அவரால் படைக்க இயலவில்லை, <br/>
நின் பேனா தீட்டிய வரியை, நின் கையழுத்திட்டுப் படைத்தாய். <br/>

உறவினர் இறந்தப்போது துயர் கொள்ளாத இதயம், <br/>
உன் மரணத்தில் கனக்கிறது.<br/>

துனிந்து முழங்க சொற்களையும், <br/>
பயமின்றி நடையிடப் பாதையையும், <br/>
விட்டுச் சென்றிருக்கிறாய், <br/>
தமிழுள்ள வரை உனக்கு மரணமில்லை. <br/>


When I was barely ten, I started discussing politics in the school seated at the last bench with three others. 
Two of us were Kalaignar’s fan at that time. I'm still fan of his works. 
His wit, rhetoric, depth of rationality, colossal Tamil language knowledge amazed me all the time. 
I have my differences, and will miss an admired icon.






