+++
date = "2018-07-31T02:05:21+05:30"
title = "Divya"
tags = ["free-verse"]
+++


As a teenager, you always smiled,<br/>
Laughed aloud whenever possible, <br/>
And filled with happiness on all days,<br/>
Whether an easy day or hard day.<br/>

Victory or failure didn’t bother you,<br/>
The class first or class second made no difference to you,<br/>
The blows from the teacher’s cane, <br/>
Could last till the period end.<br/>
When the teacher departed,<br/>
The smile again arrived on your face.<br/>

Everyone envied you,<br/>
How could you be joyful always?<br/>
How could you stay calm under stress?<br/>
How could you care for everyone?<br/>

When the city was flooded, <br/>
You helped citizens in distress,<br/>
Cleaned the streets,<br/>
Segregated the trash,<br/>
Restored the normality.<br/>

On the women’s day trek,<br/>
A wildfire out broke,<br/>
Your life was at risk,<br/>
You rescued your mates,<br/>
And saved their lives - <br/>
Selfless good deed on a helpless day.<br/>

The hungry wildfire <br/>
Burnt down the trees,<br/>
Burnt down your skin,<br/>
Burnt down your eyes,<br/>
Burnt down your organs,<br/>
Burnt down your body,<br/>
Burnt down your life.<br/>

But your smile remains untouched,<br/>
Now I understand why you always smiled,<br/>
The smile was your life,<br/>
Be it inside or outside of you,<br/>
Your smile is etched in eternity.<br/>
