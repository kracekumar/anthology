+++
date = "2018-03-11T22:35:21+05:30"
title = "bird"
tags = ["free-verse"]
+++

You fly in the morning<br/>
You fly in the afternoon<br/>
You fly in the evening<br/>
You fly in the night<br/>

You fly on top of the pool<br/>
You fly on top of the hut<br/>
You fly on top of the farmland<br/>
You fly on top of the ambulance<br/>

You rest on a tree<br/>
You rest on balcony gate<br/>
You rest on parliament terrace<br/>
You rest on the river bed<br/>

You’re free to visit the school<br/>
You’re free to visit the temple<br/>
You’re free to enter chief minister’s house<br/>
You’re free to cross the border<br/>

You feed on fruits<br/>
You feed on rice<br/>
You feed on worms<br/>
You feed on meat<br/>

Your cry is music<br/>
Your chirp is rhythm<br/>
Your call is melodious<br/>
Your song is attractive<br/>

You fly in summer<br/>
You fly in winter<br/>
You fly in rains<br/>
You fly in spring<br/>

Can we exchange our lives for a day?
