+++
date = "2019-12-27T11:15:21+05:30"
title = "Rowing Between the Rooftops"
tags = ["book review", "keralafloods"]
+++

<img src="https://www.speakingtigerbooks.com/wp-content/uploads/2019/08/9789388326827.jpg">

Everybody once or multiple times must have encountered one or all of the following questions.

- What’s the worth of human life?
- What’s the purpose of life?
- What’s the meaning of life?

The answers to these questions vary largely on the various human capacities, reasons, and faith. And not definitive.

<img src="https://upload.wikimedia.org/wikipedia/commons/a/a3/SNC_Initiates_Operation_Madad_in_Kerala-opmadad70.jpg">

In the last three years in South India, few crore individuals and beings had been threatened by natural disasters: Chennai Floods - 2016, Oukhi Cyclone - 2017, and Kerala Floods 2018. 


In 2018, during the few days of the deluge, the whole living turned upside down in Kerala. In the book, **“Rowing Between the Rooftops,”** 
the author, **Rejimon Kuttappan**, captures the spirit and selfless, brave act of Kerala fishermen in the times of utmost need.
The ten chapters in the book, lucidly tells the events of bravery, experience of the rescue, reactions of the affected humans, 
how each fisherman put their life on the boat leaving behind the economic distress of their lives with one mission to save lives, 
and how victims react in the most unexpected line of religion and caste. 

Approximately, the union government rescue forces saved 8000 lives, whereas these fishermen saved 65000 lives. 
These fishermen swam and rode the boats across all the odds(electric lines, snakes, crocodiles, or rumors of the reptiles floating and whatnot). 
Not worrying protocols, with one mission to accomplish, *save the human lives*. 
Throughout the book, every fisherman encountered the harshest situation and not even once failed to risk their life. 
Each one putting aside their physical limitations and pains, they went on, again and again, 
to save lives to all possible extents - carrying the pregnant lady, specially-abled, children, and elderly on their back, 
as if the pains are washed away by the currents while rescuing.

During personal conversations, every now and then, someone would say, caste is past and dead in modern times. 
Maybe some are ignorant, some can’t distinguish class and caste, and some deliberate ignore facts. 
In one of the chapters, **“caste and callousness,”** a fisherman went to rescue a family, 
they refused to board the boat, the reason of the brahmin lady is, 
**“the fishermen are from lower castes, converted to Christianity, boarding a boat is an act of impurity.”**
An individual puts the caste before their life. 

It’s apt to cite here one of Dr. B.R Ambedkar’s writings, 

> “Caste is a state of mind. It is a disease of the mind. 
> The teachings of the Hindu religion are the root cause of this disease. 
> We practice casteism, and we observe Untouchability because we are enjoined to do so by the Hindu religion. 
> A bitter thing cannot be made sweet. 
> The taste of anything can be changed. But poison cannot be changed into nectar.” 


<img src="https://upload.wikimedia.org/wikipedia/commons/5/53/Kerala_flood_2018.jpg">

During the flood times, like most of the people, I had a tab on social media accounts and keralarescue.in to know the status of the rescue. 
I called up all my friends, enquired about the status. 
Most of them and their families who are well off, already found places to stay in camps. 
Later in the same year, in Chennai, I meet a restaurant manager from Kerala. 
Slowly during the conversation, he said, his three years family income was washed away during the flood. 
All over the world, marginalized people are vulnerable to natural disasters. 

In recent times, during protests and natural disasters, social media played a pivotal role in rescue efforts. 
This pattern was visible during the Chennai floods too. 
A person sitting in any part of the country, finding the posts in social media, collecting all the details in a single place, 
verifying the authenticity and duplicity, and passing it to the folks in the ground has become truly possible 
because of the decentralized nature of the internet. 
As a matter of fact, more than 100 folks contributed to keralarescue.in web application
(https://github.com/IEEEKeralaSection/rescuekerala/graphs/contributors) which was pivotal in rescue operations carried out by the state.
 
Indians are poor at documenting their history. Once Annadurai, DMK founder, visited the US and said in similar lines, 
“the majority of US museums artifacts are less than three years old, but in India, the artifacts date before Common Era, 
yet there are no world-class museums.” 
Rejimon has become the voice of the voiceless and neglected fishermen in the rescue effort. Along with the incidents, 
the author mentions the history and background of the place, fishermen, society, and the state. As a result, the book reaches closer to any reader. 
When the rescue efforts were on, everyone remembered them. After the rescue, there were videos of fishermen send-off, where the crowd sang songs. 
I don’t know, when was the last time after the floods, I gave thought about the fishermen. 
Life moves on, heroic, selfless acts are forgotten. 
All the testimony of the fishermen, convey, “No amount of money, medals can satisfy the earnest gratitude and fulfillment of saving a life.”

In one of the testimony, a fisherman, Joseph, narrates the experience of rescuing a pregnant lady. 
At that time, the boat was almost full. The rescuers were Christians and Muslims. The boat mates refused to give a place to a pregnant lady. 
After losing the patience, Joseph shouted at others, “I’m here to save lives. I’ll bring the pregnant lady to boat. Everyone agreed finally”. 
After narrating the incident, Joseph questions the author, 
“Why do rich people behave like these? They live in two-storey building and behave less compassionate. We’re poor but never ditch our community.” 
The author refuses to generalize and reply. This is brilliant and non-judgemental, impossible to acquire without practice! 
At this point, the writing becomes the medium between Joseph and the reader. It’s a remarkable style.

There is always a study about the disaster in the lines of economic loss. 
It’s the writers who put the less talked events in the so-called mainstream discussion. 
With this book, the author has put the coastal warriors in mainland history. 
History is always about empires falling and rise, what about the history of normal individuals rising above the odds to the occasion? 
It’s heartwarming to read, Shashi Tharoor has nominated, fishermen to Nobel peace prize.

The book is written in an accessible and straightforward manner. 
You can read it in a single sitting. The page after page, you can witness the generosity, humanness, bravery of the fishermen folks. 
Kudos for the author for writing it. 

In all the testimonies, fishermen worked together in a team. 
They brought their own or borrowed the boats. In the end, boats were all damaged. 
The state government couldn’t subsidize or reimburse the entire fuel expense. 
All these men are day laborers, burnt money from their pockets. 
Sometimes no dress to change on the next day. They all were living in poverty and risked everything they can.

The one who has nothing to lose has everything to gain.