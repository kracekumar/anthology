+++

date = "2017-02-12"
title = "About"
menu = "nav"

+++

The site contains non-technical posts about life and observations. Technical writing lies in [kracekumar.com](http://kracekumar.com).
